//
//  Constants.swift
//  homeCheif
//
//  Created by mohamed abdo on 4/21/18.
//  Copyright © 2018 Atiaf. All rights reserved.
//
import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
struct Constants{
    
    static let locale = LocalizationHelper.getLocale()
    static var login:String{
        get{
            Constants.storyboard = Storyboards.main.rawValue
            return "Intro"
        }
    }
    static var storyboard = Storyboards.main.rawValue
    static let url = "https://onnety-solutions.com/gotoodrive/api/v1/"
    static let companyUrl = "http://80.241.219.167/~godrive"
    static let copyrightUrl = ""
    static let itunesURL = "itms-apps://itunes.apple.com/app/id1330387425"
    static let version = "v1"
    static let deviceType = "2"
    static let deviceToken = "deviceToken"
    static let deviceId = UIDevice.current.identifierForVendor!.uuidString
    static let googleAPI = "AIzaSyBdGenvpfaqEvMkxiyJ7citsVHPf-dxTUM"
    static let googleRoutesAPI = "AIzaSyBjqyiRsAR5FtV2ZFFL3H8rSymPuUmGQHw"
    
    static let mainColorRGB = UIColor(red: 244/255, green: 154/255, blue: 0/255, alpha: 1)
    static let textColorRGB = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    static let borderColorRGB = UIColor.init(red: 209/255, green: 209/255, blue: 209/255, alpha: 1)
    static let underlineRGB = UIColor(red: 209/255, green: 209/255, blue: 209/255, alpha: 1)
    
    static var splash:Void!
    static func sleep(time:TimeInterval){
        Constants.splash = Thread.sleep(forTimeInterval: time)
        
    }
    static func initAppDelegate(){
        initLang()
        //Constants.sleep(time: 3)
        //Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey(Constants.googleAPI)
        GMSPlacesClient.provideAPIKey(Constants.googleAPI)
    }
    
    
}


public enum Storyboards:String{
    case main = "Main"
}


public enum PaymentsMethod :String{
    case cash
    case paypal
    case credit
}

public enum Apis:String {
    case configs
    case models
    case sub_categories
    case token = "refresh/token"
    case forget_password = "password/reset"
    case reset_password = "password/verify"
    case login = "driver/login"
    case register_step1 = "driver/register/step1"
    case register_step2 = "driver/register/step2"
    case social = "driver/register/social"
    case update = "driver/update"
    case logout
    case current_trip = "trips/driver/current"
    case accept = "trips/accept"
    case reject = "trips/reject"
    case start = "trips/start"
    case arrive = "trips/arrive"
    case startLearn = "trips/start/learn/class"
    case endLearn = "trips/end/learn/class"
    case complete = "trips/complete"
    case collect = "trips/collecting"
    case rate = "driver/trip/rate"
    case history_rates = "driver/rates"
    case history = "driver/history"
    case notifications = "driver/notifications"
    case payto_admin = "driver/payto/admin"
    case request_from_admin = "driver/request/admin"
    case transactions = "driver/transactions"
    case home = "driver/home"
    case category_attachments = "category/attachment"
    case cancel_request = "trips/cancel"
    case chat = "my/rooms/detail"
    case send_message = "my/rooms/send"

}


extension AppDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Constants.initAppDelegate()
        setupFirebase()
        return true
    }
}
