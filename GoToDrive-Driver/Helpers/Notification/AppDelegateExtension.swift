//
//  AppDelegateExtension.swift
//  Etihad-Member
//
//  Created by mohamed abdo on 3/20/19.
//  Copyright © 2019 Onnety. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Firebase
import UserNotifications

fileprivate let gcmMessageIDKey = "gcm.message_id"
typealias SoundHandler = (Bool) -> ()
extension AppDelegate: FirebaseNotificationDelegate {
    func notificationControl(notification: [AnyHashable : Any], closure: SoundHandler? = nil ) {
        let string = notification["gcm.notification.extras"] as? String
        let noti = string?.data(using: .utf8)
        
        /** check chat message **/
        let message = ChatResult.convertToModel(response: noti)
        message.result?.from_you = false
        message.result?.user?.image = message.sender_image
        self.chatDelegate?.didReceive(message: message.result)

        /** end **/
        /** check trip message **/
        let trip = TripResultNotification.convertToModel(response: noti)
        self.tripDelegate?.didReceive(trip: trip.result)
        /** end **/
        if message.id != nil {
            if (UIApplication.topViewController() as? Chat) != nil {
                closure?(false)
            } else {
                closure?(true)
            }
        } else {
            closure?(true)
        }
        
    }
    func fetchToMessage(data: Any?) {
        
//        if message.type == 2 {
//            let storyboard = UIStoryboard(name: Storyboards.main.rawValue, bundle: nil)
//            let nav = storyboard.instantiateViewController(withIdentifier: "ChatNav") as? UINavigationController
//            guard let chat = nav?.rootViewController as? Chat else { return }
//            chat.userID = message.result?.user_id?.int
//            chat.chatName = message.result?.name
//            self.window?.rootViewController = nav
//            self.window?.makeKeyAndVisible()
//        }
//
        //let message = ChatModel
    }
}

extension AppDelegate {
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        print("hiITEM")
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        //        Messaging.messaging().subscribe(toTopic: "/topics/mp_client_ios")
        Messaging.messaging()
            .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        Messaging.messaging()
            .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
}

//device token delegates
extension AppDelegate : MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.setValue(fcmToken, forKey: Constants.deviceToken)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID  = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        //print("here it  is\(userInfo)")
        
        notificationControl(notification: userInfo) { sound in
            if sound {
                completionHandler([.alert, .badge, .sound])
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        notificationControl(notification: userInfo)
        completionHandler()
    }
}

