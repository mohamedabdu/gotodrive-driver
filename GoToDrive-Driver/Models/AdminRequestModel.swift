
import Foundation

class AdminRequestModel : Decodable{
    
    var result : AdminRequestResult?
    var statusCode : Int?
    var statusText : String?
    var message : String?

    
    public static func convertToModel(response: Data?) -> AdminRequestModel{
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return AdminRequestModel()
        }
    }
    
    
}

class AdminRequestResultModel : Decodable{
    
    var result : [AdminRequestResult]?
    var statusCode : Int?
    var statusText : String?
    var message : String?
    
    
    public static func convertToModel(response: Data?) -> AdminRequestResultModel{
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return AdminRequestResultModel()
        }
    }
    
    
}
class AdminRequestResult : Decodable{
    var id:Int?
    var admin_credit:Double?
    var driver_credit:Double?
    var driver_id:Int?
    var type_text:String?
    var message:String?
    var type:String?
    var created_at:String?

}
