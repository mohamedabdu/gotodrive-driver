
import Foundation

class CategoryAttachmentModel : Codable {
    
    var result : [CategoryAttachmentResult]?
    var statusCode : Int?
    var statusText : String?
    var message : String?

    
    public static func convertToModel(response: Data?) -> CategoryAttachmentModel {
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return CategoryAttachmentModel()
        }
    }
    
    
}

class CategoryAttachmentResult : Codable {
    var id: Int?
    var name: String?
    var img: String?
    var attachment_id: Int?
    var key: String?
}
