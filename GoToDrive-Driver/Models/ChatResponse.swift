//
//	ChatResponse.swift
//
//	Create by mohamed abdo on 11/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class ChatResult: Decodable {
    
    var id: Int?
    var room_id : Int?
	var from_you : Bool?
	var message : String?
	var user : ChatUser?
    var created_at: String?
    var result: ChatResult?
    var trip_id: String?
    var sender_name: String?
    var sender_image: String?
    var type: Int?
    var from: Int?
	public static func convertToModel(response: Data?) -> ChatResult {
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return ChatResult() 
		}
 	}
//{"result":{"room_id":115,"is_read":null,"created_at":"2019-03-24 15:56:28","from":452,"id":563,"to":461,"message":"j","user":{"image":"http:\/\/80.241.219.167\/~godrive\/public\/uploads\/no_avatar.jpg","mobile":"123456123","last_name":"mohamed","id":461,"first_name":"mohamed","email":"moha@moha.com"},"from_you":true},"trip_id":"2015","sender_name":"test iejriweo","from":452,"to":"461","type":2}

}
