//
//	ChatUser.swift
//
//	Create by mohamed abdo on 11/10/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class ChatUser : Decodable{
    var id: Int?
	var first_name : String?
	var last_name : String?
	var email : String?
	var mobile : String?
	var image : String?
}
