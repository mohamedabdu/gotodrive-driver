//
//	ConfigBrand.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class ConfigBrand : Decodable{

	var id : Int?
	var image : String?
	var name : String?
    var type : String? // type = X type = N type = delivery
    var sub_category_id : Int?
    var category_id : Int?


	public static func convertToModel(response: Data?) -> ConfigBrand{
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return ConfigBrand() 
		}
 	}


}
