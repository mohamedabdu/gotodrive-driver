//
//	SubCategoryResult.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 

class Driver : Decodable{
    
    var admin_credit : Double?
    var car : DriverCar?
    var car_license_img : String?
    var category_icon: String?
    var category_id : Int?
    var category_name : String?
    var category_calculating_pricing: String?
    var driving_license_img : String?
    var his_credit : Double?
    var id : Int?
    var lat : String?
    var lng : String?
    var online : Bool?
    var statistics : DriverStatistic?
    var currentRequest:CurrentRequest?
    var currentTrip:TripResult?
    var total_credit : Double?
    var rate:String?
    
}
class DriverStatistic : Decodable{
    var online_hours : Int?
    var today_earned : Int?
    var today_trips : Int?
}

class CurrentRequest : Decodable{
    var id : Int?
    var accept : Int?
    var driver : User?
    var price : Double?
    var type : Int?
    var type_text : String?
    var trip_orgin : String?
    var trip_destination : String?
    var time_estimation : String?
    var fare_estimation : String?
    var from_lat : Double?
    var from_lng : Double?
    var to_lat : Double?
    var to_lng : Double?
    var trip_special_description:String?
    var trip_special_notes:String?
    var trip_delivery_note: String?
    
}
