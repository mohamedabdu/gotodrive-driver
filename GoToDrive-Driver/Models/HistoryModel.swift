//
//	HistoryModel.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class HistoryModel : Decodable {

	var result : HistoryResult?
	var statusCode : Int?
	var statusText : String?


	public static func convertToModel(response: Data?) -> HistoryModel{
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return HistoryModel() 
		}
 	}


}
