//
//	HistoryStatistic.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class HistoryStatistic : Decodable{
	var count_trips : Int?
	var spend_time : String?
	var total_earned : Double?
    var weeklyEarning:Double?
    var weeklyDriver:Double?
    var weeklyAdmin:Double?
}
