//
//	HistoryToday.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class HistoryToday : Decodable{

	var statistics : HistoryStatistic?
	var trips : [TripResult]?
}
