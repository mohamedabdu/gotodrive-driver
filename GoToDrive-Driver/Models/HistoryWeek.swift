//
//	HistoryWeek.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class HistoryWeek : Decodable{

	var statistics : HistoryStatistic?
    var trips : [TripResult]?
}
