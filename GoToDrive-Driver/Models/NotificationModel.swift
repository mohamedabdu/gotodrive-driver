
import Foundation

class NotificationsModel : Decodable{
    
    var result : [NotificationsResult]?
    var statusCode : Int?
    var statusText : String?
    
    
    public static func convertToModel(response: Data?) -> NotificationsModel{
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return NotificationsModel()
        }
    }
    
    
}
class NotificationsResult : Decodable{
    
    var title:String?
    var body:String?
}
