//
//	ReviewModel.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class ReviewModel : Decodable{

	var result : ReviewResult?
	var statusCode : Int?
	var statusText : String?


	public static func convertToModel(response: Data?) -> ReviewModel{
 		do{ 
 			let data = try JSONDecoder().decode(self, from: response!)
 			return data 
 		}catch{ 
 			return ReviewModel() 
		}
 	}


}