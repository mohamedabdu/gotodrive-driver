//
//	ReviewRate.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class ReviewRate : Decodable{

	var client_image : String?
	var client_name : String?
	var comment : String?
	var end_date : String?
	var rate : Int?

}
