//
//	ReviewResult.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 



class ReviewResult : Decodable{

	var avg_rate : Int?
	var count_rates : Int?
	var rates : [ReviewRate]?
	var statistics_text : ReviewStatistic?
}
