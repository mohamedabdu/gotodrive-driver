//
//	TripResult.swift
//Created By Abdu Exporter. All rights reserved.


import Foundation 

class TripModel : Decodable{
    
    var result: [TripResult]?
    var current_request: TripResult?
    
    public static func convertToModel(response: Data?) -> TripModel{
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return TripModel()
        }
    }

}

class TripResult : Decodable{
    
    var category_calculating_price: String?
	var category_id : Int?
    var category_name : String?
    var sub_category_id : Int?
	var sub_category_name : String?
	var client_comment : String?
	var client_rate : Int?
    var driver_comment : String?
    var driver_rate : Int?
	var commission : Double?
	var created_at : String?
    var wallet : Double?
    var country_tax : Double?
	var discount : Double?
	var distance : Double?
	var client : User?
	var end_date : String?
	var fare_estimation : String?
	var from_lat : Double?
	var from_lng : Double?
	var from_location : String?
	var id : Int?
	var payment_method : Int?
	var payment_method_text : String?
    var payment_paid:Bool?
	var statistics : TripStatistic?
	var status : Int?
	var status_text : String?
	var tax : Double?
	var time_estimation : String?
	var to_lat : Double?
	var to_lng : Double?
	var to_location : String?
	var total_price : Double?
	var trip_price : Double?
	var type : String?
    var trip_special_description:String?
    var trip_special_notes:String?
    var learn_status: Int?
    var learn_status_text: String?
    var trip_delivery_note: String?
    // var requests: [CurrentRequest]?
}

class TripResultNotification : Decodable {
    
    var result: TripResult?
    public static func convertToModel(response: Data?) -> TripResultNotification {
        do{
            let data = try JSONDecoder().decode(self, from: response!)
            return data
        }catch{
            return TripResultNotification()
        }
    }
}
