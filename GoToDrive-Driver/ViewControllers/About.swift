//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class About:BaseController {
    
    @IBOutlet weak var helpText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        BaseController.configLoaded = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func setup() {
        helpText.text = BaseController.config?.config?.about_us
    }
    override func notifySetting() {
        helpText.text = BaseController.config?.config?.about_us
    }
    override func bind() {
        
    }
}
