//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Account:BaseTabBar {
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    var viewModel:UserViewModel?
    var imagePicker:ImagePickerHelper?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.currentIndex = .account
        super.viewDidLoad()
        setup()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    
    override func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
        
        let user = UserRoot.getUserFromCache()
        if user.result?.driver?.car?.car_image == nil {
            carImage.image = UIImage(named: "carB")
        }else{
            carImage.setImage(url: user.result?.driver?.car?.car_image)
        }
        userImage.setImage(url: user.result?.image)
        userName.text = "\(user.result?.first_name ?? "") \(user.result?.last_name ?? "")"
        
    }
    override func bind() {
        
    }
    @IBAction func changeImage(_ sender: Any) {
        imagePicker = ImagePickerHelper(self)
    }
    @IBAction func editProfile(_ sender: Any) {
        let vc = pushViewController(EditProfile.self)
        push(vc)
    }
    @IBAction func documents(_ sender: Any) {
    }
    @IBAction func notifications(_ sender: Any) {
        let vc = pushViewController(Notifications.self)
        push(vc)
    }
    @IBAction func settings(_ sender: Any) {
        let vc = pushViewController(Settings.self)
        push(vc)
    }
    @IBAction func about(_ sender: Any) {
        let vc = pushViewController(About.self)
        push(vc)
    }
    @IBAction func help(_ sender: Any) {
        let vc = pushViewController(Help.self)
        push(vc)
    }
    @IBAction func logout(_ sender: Any) {
        if UserRoot.isLogin() {
            UserRoot.removeCacheingDefault()
            let vc = pushViewController(indetifier: Constants.login)
            push(vc)
        }
    }
    @IBAction func payments(_ sender: Any) {
        let vc = pushViewController(Payments.self)
        push(vc)
    }
}

extension Account:ImagePickerDelegate{
    func pickerCallback(image: UIImage) {
        self.userImage.image = image
        var paramters:[String:Any] = [:]
        paramters["image"] = image.base64(format: .JPEG(0.1))
        self.viewModel?.update(paramters: paramters)
    }
}
