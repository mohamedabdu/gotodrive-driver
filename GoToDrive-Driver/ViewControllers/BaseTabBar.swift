//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class BaseTabBar:BaseController {

    enum TabBarIndex {
        case home
        case earn
        case rate
        case account
    }
    @IBOutlet weak var homeUnderline: UIView!
    @IBOutlet weak var earningUnderline: UIView!
    @IBOutlet weak var ratingUnderline: UIView!
    @IBOutlet weak var accountUnderline: UIView!
    
    @IBOutlet weak var driverStatus: UISwitch!
    @IBOutlet weak var statusText: UILabel!
    
    var tabUserViewModel:UserViewModel?
    var currentIndex:TabBarIndex = .home
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTab()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        tabUserViewModel = nil
    }
    func setup(){
        
    }
    func setupTab(){
        if self.currentIndex == .home {
            homeUnderline.isHidden = false
            earningUnderline.isHidden = true
            ratingUnderline.isHidden = true
            accountUnderline.isHidden = true
        }else if self.currentIndex == .earn {
            homeUnderline.isHidden = true
            earningUnderline.isHidden = false
            ratingUnderline.isHidden = true
            accountUnderline.isHidden = true
        }else if self.currentIndex == .rate {
            homeUnderline.isHidden = true
            earningUnderline.isHidden = true
            ratingUnderline.isHidden = false
            accountUnderline.isHidden = true
        }else {
            homeUnderline.isHidden = true
            earningUnderline.isHidden = true
            ratingUnderline.isHidden = true
            accountUnderline.isHidden = false
        }
        tabUserViewModel = UserViewModel()
        guard let online = UserRoot.getUserFromCache().result?.driver?.online else { return }
        if online {
            driverStatus.isOn = true
            statusText.text = translate("online")
        }else{
            driverStatus.isOn = false
            statusText.text = translate("offline")
        }
    }
    @IBAction func changeStatus(_ sender: Any) {
        if driverStatus.isOn {
            statusText.text = translate("online")
        }else{
            statusText.text = translate("offline")
        }
        guard let category = UserRoot.getUserFromCache().result?.driver?.category_id else { return }
        var paramters:[String:Any] = [:]
        paramters["category_id"] = category
        if driverStatus.isOn {
            paramters["online"] = 1
        }
        self.tabUserViewModel?.update(paramters: paramters)
        
        
    }
    
    @IBAction func homeBtn(_ sender: Any) {
        let vc = pushViewController(Home.self)
        push(vc,false)
    }
    
    @IBAction func earningBtn(_ sender: Any) {
        let vc = pushViewController(Earning.self)
        push(vc,false)
    }
    @IBAction func ratingBtn(_ sender: Any) {
        let vc = pushViewController(Rating.self)
        push(vc,false)
    }
    
    @IBAction func accountBtn(_ sender: Any) {
        let vc = pushViewController(Account.self)
        push(vc,false)
    }
}
