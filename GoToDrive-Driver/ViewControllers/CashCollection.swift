//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import Cosmos

class CashCollection:BaseController {
   
    @IBOutlet weak var totalFare: UILabel!
    @IBOutlet weak var totalDistance: UILabel!
    @IBOutlet weak var orginLocation: UILabel!
    @IBOutlet weak var destinationLocation: UILabel!
    @IBOutlet weak var tripFare: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var outstanding: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var tripType: UILabel!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var nashmiBill: UILabel!
    @IBOutlet weak var billTax: UILabel!
    
    var trip:TripResult?
    var viewModel:TripViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        setup()
        setupTrip()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = TripViewModel()
        totalFare.text = translate("0","SAR",true)
        totalDistance.text = translate("0", "km")
        orginLocation.text = ""
        destinationLocation.text = ""
        tripFare.text = translate("0","SAR",true)
        discount.text = translate("0","SAR",true)
        outstanding.text = translate("0","SAR",true)
        totalPrice.text = translate("0","SAR",true)
    }
    func setupTrip(){
        totalFare.text = translate(trip?.total_price?.string,"SAR")
        totalDistance.text = translate(trip?.distance?.string, "km")
        orginLocation.text = trip?.from_location
        destinationLocation.text = trip?.to_location
        tripFare.text = translate(trip?.trip_price?.string,"SAR")
        discount.text = translate(trip?.discount?.string,"SAR")
        outstanding.text = translate(trip?.wallet?.string,"SAR")
        totalPrice.text = translate(trip?.total_price?.string,"SAR")
        tripType.text = translate(trip?.sub_category_name ?? "")
        tripDate.text = translate(trip?.created_at ?? "")
        nashmiBill.text = translate(trip?.tax?.string,"SAR")
        billTax.text = translate(trip?.country_tax?.string,"SAR")
    }
    override func bind() {
        viewModel?.message.bind({ (message) in
            self.backBtn(self)
        })
    }
    
    @IBAction func cashCollected(_ sender: Any) {
        viewModel?.collect()
        let vc = pushViewController(ReviewTrip.self)
        vc.trip = self.trip
        push(vc)
    }
}
