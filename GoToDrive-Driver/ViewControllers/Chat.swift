//
//  Login.swift
//  Drstyle
//
//  Created by mohamed abdo on 9/24/18.
//  Copyright © 2018 StopGroup. All rights reserved.
//

import Foundation

class Chat:BaseController {
    
    
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chatCollection: UITableView!
    
    var friendID: Int!
    var tripID: Int!
    
    var lastIndex:IndexPath!
    var up: Bool = false
    var chatName: String?
    var viewModel: TripViewModel?
    var messages: [ChatResult] = []
    var messageModel: ChatResult?
    var request: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let _ = friendID, let _ = tripID else { return }
        setup()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.chatDelegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.chatDelegate = nil
    }
    
    func setup() {
        userName.text = chatName
        viewModel = TripViewModel()
        // viewModel?.delegate = self
        viewModel?.chatRoom(userID: friendID, trip: tripID)
        chatCollection.delegate = self
        chatCollection.dataSource = self
        
        if getAppLang() == "ar" {
            messageField.addPaddingRight(5)
        } else {
            messageField.addPaddingLeft(5)
        }
    }
    func reload(){
        chatCollection.reloadData {
            if !self.up {
                if self.chatCollection.contentSize.height >= self.view.frame.height {
                    self.chatCollection.scrollToBottom()
                }
            }else{
                if self.lastIndex != nil {
                    self.chatCollection.scrollToRow(at:self.lastIndex , at: UITableViewScrollPosition.none, animated: true)
                }
            }
        }
    }
    override func bind() {
        
        viewModel?.chatHandler.bind({ (respond) in
            var chats: [ChatResult] = []
            chats.append(contentsOf: respond)
            chats.reverse()
            if !self.up {
                self.messages.append(contentsOf: chats)
            } else {
                let messages = self.messages
                self.messages = chats
                self.messages.append(contentsOf: messages)
            }
            self.reload()
        })
        
    }
    
    override func validation() -> Bool {
        let validate = Validation(textFields: [messageField])
        return validate.success
    }
   
    @IBAction func sendMessage(_ sender: Any) {
        let user = UserRoot.getUserFromCache()
        guard let replay = messageField.text else{return}
        if !self.validation(){
            return
        }
        if replay.isEmpty {
            return
        }
        
        
        self.viewModel?.sendMessage(userID: friendID, trip: tripID, message: replay)
        
        messageModel = ChatResult()
        messageModel?.created_at = Date.date(date: Date.current(), type: .full)
        messageModel?.user = ChatUser()
        messageModel?.user?.first_name = user.result?.first_name
        messageModel?.user?.last_name = user.result?.last_name
        messageModel?.user?.image = user.result?.image
        messageModel?.message = messageField.text
        messageModel?.from = user.result?.id
        // messageModel?.to = friendID
        messageModel?.from_you = true
        if messageModel != nil {
            messages.append(messageModel!)
            self.reload()
        }
        messageField.text = ""
    }
    
    override func backBtn(_ sender: Any) {
        self.navigationController?.pop()
    }
}

extension Chat:UITableViewDelegate,UITableViewDataSource{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y <= 0 && viewModel!.runPaginator(){
            self.up = true
            viewModel?.chatRoom(userID: friendID, trip: tripID)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.lastIndex = indexPath
        if messages[indexPath.row].from_you != nil && messages[indexPath.row].from_you!{
            guard var cell = tableView.cell(type:ChatToCell.self, indexPath) else { return UITableViewCell() }
            cell.model = messages[indexPath.row]
            return cell
           
        }else {
            guard var cell = tableView.cell(type:ChatFromCell.self, indexPath) else { return UITableViewCell() }
            cell.model = messages[indexPath.row]
            return cell
        }
        
    }
    
    
}


// MARK: - chat Delegate to Appdelegate
protocol ChatDelegate: class {
    func didReceive(message: ChatResult?)
}
fileprivate weak var _chatDelegate: ChatDelegate?
extension AppDelegate {
    weak var chatDelegate: ChatDelegate? {
        set {
            _chatDelegate = newValue
        } get {
           return _chatDelegate
        }
    }
}

// MARK: - ChatDelegate
extension Chat: ChatDelegate {
    func didReceive(message: ChatResult?) {
        guard let message = message else { return }
        messages.append(message)
        self.reload()
    }
}
