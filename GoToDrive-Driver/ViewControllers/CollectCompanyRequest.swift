//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class CollectCompanyRequest:BaseController {
    @IBOutlet weak var totalDistance: UILabel!
    @IBOutlet weak var totalFare: UILabel!
    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var timeArrive: UILabel!
    @IBOutlet weak var distance: UILabel!
    
    @IBOutlet weak var tripType: UILabel!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var nashmiBill: UILabel!
    @IBOutlet weak var billTax: UILabel!
    @IBOutlet weak var tripFare: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var tripTotal: UILabel!
    
    
    var trip:TripResult?
    var viewModel:TripViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        setup()
        setupTrip()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = TripViewModel()
        totalFare.text = translate("0","SAR")
        totalDistance.text = translate("0", "km")
    }
    func setupTrip(){
        totalFare.text = translate(trip?.fare_estimation,"SAR")
        totalDistance.text = translate(trip?.distance?.string, "km")
        clientName.text = "\(trip?.client?.first_name ?? "") \(trip?.client?.last_name ?? "")"
        clientImage.setImage(url: trip?.client?.image)
        timeArrive.text = trip?.time_estimation
        distance.text = "\(trip?.distance?.string ?? "") km"
        
        tripFare.text = translate(trip?.trip_price?.string,"SAR")
        discount.text = translate(trip?.discount?.string,"SAR")
        tripTotal.text = translate(trip?.fare_estimation,"SAR")
        tripType.text = translate(trip?.sub_category_name ?? "")
        tripDate.text = translate(trip?.created_at ?? "")
        nashmiBill.text = translate(trip?.tax?.string,"SAR")
        billTax.text = translate(trip?.country_tax?.string,"SAR")
        
    }
    override func bind() {
        viewModel?.collecting.bind({ (data) in
            let vc = self.pushViewController(Home.self)
            self.push(vc)
        })
    }
    
    @IBAction func help(_ sender: Any) {
        let vc = pushViewController(Help.self)
        push(vc)
    }
    
    @IBAction func collecting(_ sender: Any) {
        viewModel?.delegate = self
        viewModel?.collect(trip: trip?.id)
    }
}
