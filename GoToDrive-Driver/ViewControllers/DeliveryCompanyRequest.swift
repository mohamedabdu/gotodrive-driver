//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps
import MapKit
class DeliveryCompanyRequest:BaseController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var toDestination: UILabel!
    
    var mapHelper:GoogleMapHelper?
    var lat:Double?
    var lng:Double?
    var address:String?
    
    /** options **/
    /** if map load update camera manual */
    var mapLoaded:Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
    }

    func setup() {
        mapHelper = GoogleMapHelper()
        mapHelper?.mapView = mapView
        mapHelper?.drawMarkerOnLocationChanged = false
        mapHelper?.moveCameraOnUpdateLocation = false
        mapHelper?.changeCameraOnMarker = false
        mapHelper?.clearMapOnUpdateLocation = false
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        if let lati = lat , let longi = lng {
            mapHelper?.updateCamera(lat: lati, lng: longi)
            mapHelper?.setMarker(position: CLLocationCoordinate2D(latitude: lati, longitude: longi))
        }
        toDestination.text = address
        
    }
   
    override func bind() {
    }
    
    @IBAction func deliverMe(_ sender: Any) {
        openMapForPlace()
    }
    func getDeliveryOption()->(Double,Double,String?){
        if let lati = lat , let longi = lng {
            return (lati , longi ,address)
        }else{
            return (0 , 0 ,address)
        }
    }
    func openMapForPlace() {

        let installedNavigationApps : [String] = ["Apple Maps","Google Maps"] // Apple Maps is always installed
    
        let alert = UIAlertController(title: translate("alert"), message: translate("select_navigation_app"), preferredStyle: .actionSheet)
        for app in installedNavigationApps {
            let button = UIAlertAction(title: app, style: .default){ (action) in
                if action.title == "Apple Maps" {
                    self.openAppleMap()
                }else{
                    self.openGoogleMap()
                }
            }
            
            alert.addAction(button)
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    func openAppleMap(){
        let (lat, lng, title) = getDeliveryOption()
        
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = lng
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = title
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    func openGoogleMap(){
        let (lat, lng, title) = getDeliveryOption()
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?saddr=&daddr=\(lat),\(lng)&directionsmode=driving")!)
        } else {
            NSLog("Can't use comgooglemaps://");
        }
    }
}

extension DeliveryCompanyRequest:GoogleMapHelperDelegate{
    func locationCallback(lat: Double, lng: Double) {
        if !mapLoaded {
            mapLoaded = true
            self.mapHelper?.updateCamera(lat: lat, lng: lng)
            mapHelper?.setMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
            
            
        }
    }
 
    
}

/** dataSource of Map */
extension DeliveryCompanyRequest:MarkerDataSource {
    func marker() -> MarkerAttrbuite {
        var marker = MarkerAttrbuite()
        marker.use = .icon
        marker.icon = #imageLiteral(resourceName: "pinColored")
        return marker
    }
 
}
