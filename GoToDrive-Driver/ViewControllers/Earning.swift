//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import XLPagerTabStrip

protocol EarningDataSource:class {
    func earning(_ type:EarningTab.TabType)->String?
    func timing(_ type:EarningTab.TabType)->String?
    func tripsCount(_ type:EarningTab.TabType)->String?
    func collection(_ type:EarningTab.TabType)->[TripResult]?
    func statistics(_ type:EarningTab.TabType)->HistoryStatistic?
    func getViewModel()->UserViewModel?
}

class Earning:TabHostHelper {
    @IBOutlet weak var homeUnderline: UIView!
    @IBOutlet weak var earningUnderline: UIView!
    @IBOutlet weak var ratingUnderline: UIView!
    @IBOutlet weak var accountUnderline: UIView!
    @IBOutlet weak var driverStatus: UISwitch!
    @IBOutlet weak var statusText: UILabel!
    
    var viewModel:UserViewModel?
    var history:HistoryResult?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let vc = pushViewController(EarningTab.self)
        vc.pageType = .today
        vc.dataSoruce = self
        let vc1 = pushViewController(EarningTab.self)
        vc1.pageType = .weekly
        vc1.dataSoruce = self
        return [vc,vc1]
    }
    
    func setup(){
        homeUnderline.isHidden = true
        earningUnderline.isHidden = false
        ratingUnderline.isHidden = true
        accountUnderline.isHidden = true
        guard let online = UserRoot.getUserFromCache().result?.driver?.online else { return }
        if online {
            driverStatus.isOn = true
            statusText.text = translate("online")
        }else{
            driverStatus.isOn = false
            statusText.text = translate("offline")
        }
        
        viewModel = UserViewModel()
        //viewModel?.delegate = self
        viewModel?.fetchHistory()
    }
    func bind() {
        viewModel?.history.bind({ (data) in
            self.history = data
            self.reloadPagerTabStripView()
        })
    }
    @IBAction func changeStatus(_ sender: Any) {
        if driverStatus.isOn {
            statusText.text = translate("online")
        }else{
            statusText.text = translate("offline")
        }
        guard let category = UserRoot.getUserFromCache().result?.driver?.category_id else { return }
        var paramters:[String:Any] = [:]
        paramters["category_id"] = category
        if driverStatus.isOn {
            paramters["online"] = 1
        }
        self.viewModel?.update(paramters: paramters)
    }
    
    @IBAction func homeBtn(_ sender: Any) {
        let vc = pushViewController(Home.self)
        push(vc,false)
    }
    @IBAction func earningBtn(_ sender: Any) {
    }
    @IBAction func ratingBtn(_ sender: Any) {
        let vc = pushViewController(Rating.self)
        push(vc,false)
    }
    
    @IBAction func accountBtn(_ sender: Any) {
        let vc = pushViewController(Account.self)
        push(vc,false)
    }
    
}

extension Earning: EarningDataSource {
    func getViewModel() -> UserViewModel? {
        return viewModel
    }
    
    func earning(_ type: EarningTab.TabType) -> String? {
        if type == .today {
            return self.history?.today?.statistics?.total_earned?.string
        }else{
            return self.history?.week?.statistics?.total_earned?.string
        }
    }
    
    func timing(_ type: EarningTab.TabType) -> String? {
        if type == .today {
            return self.history?.today?.statistics?.spend_time
        }else{
            return self.history?.week?.statistics?.spend_time
        }
    }
    
    func tripsCount(_ type: EarningTab.TabType) -> String? {
        if type == .today {
            return self.history?.today?.statistics?.count_trips?.string
        }else{
            return self.history?.week?.statistics?.count_trips?.string
        }
    }
    
    func collection(_ type: EarningTab.TabType) -> [TripResult]? {
        if type == .today {
            return self.history?.today?.trips
        }else{
            return self.history?.week?.trips
        }
    }
    func statistics(_ type: EarningTab.TabType) -> HistoryStatistic? {
        if type == .today {
            return self.history?.today?.statistics
        }else{
            return self.history?.week?.statistics
        }
    }
   
    
}
