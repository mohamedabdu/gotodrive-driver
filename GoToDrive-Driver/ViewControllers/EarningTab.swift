//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import XLPagerTabStrip

class EarningTab:BaseController {
    enum TabType:String {
        case today
        case weekly
    }
    
    @IBOutlet weak var collectionTitle: UILabel!
    @IBOutlet weak var requestAdmin: UIButton!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var tripsCollection: UITableView!
    @IBOutlet weak var completeTrip: UILabel!
    @IBOutlet weak var timeSpend: UILabel!
    @IBOutlet weak var myEarning: UILabel!
    
    var pageType:TabType = .today
    weak var dataSoruce:EarningDataSource?
    var trips:[TripResult] = []
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        trips.removeAll()
    }
    
    func setup() {
        collectionHeight.constant = 0
        completeTrip.text = ""
        timeSpend.text = ""
        myEarning.text = ""
        collectionTitle.text = translate(pageType.rawValue, "trips")
        tripsCollection.delegate = self
        tripsCollection.dataSource = self
        
        if pageType == .today {
            requestAdmin.isHidden = true
        }else{
            requestAdmin.isHidden = false
        }
    
        myEarning.text = self.dataSoruce?.earning(pageType)
        timeSpend.text = self.dataSoruce?.timing(pageType)
        completeTrip.text = self.dataSoruce?.tripsCount(pageType)
        
        guard let list = self.dataSoruce?.collection(pageType) else { return }
        self.trips.append(contentsOf: list)
        collectionHeight.constant = (list.count*90).cgFloat
        tripsCollection.isScrollEnabled = false
        tripsCollection.reloadData()
    }
    override func bind() {
      
    }
    @IBAction func request(_ sender: Any) {
        let vc = pushViewController(MakeTransaction.self)
        vc.weeklyStatistics = self.dataSoruce?.statistics(self.pageType)
        push(vc)
    }
}

extension EarningTab:IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: translate(pageType.rawValue))
    }
}

extension EarningTab:UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: EarningCell.self, indexPath) else { return UITableViewCell() }
        cell.model = trips[indexPath.row]
        return cell
    }
    
    
}
