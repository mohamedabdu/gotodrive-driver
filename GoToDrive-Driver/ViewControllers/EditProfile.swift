//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class EditProfile:BaseController {
    @IBOutlet weak var countryPicker: UIPickerView!
    @IBOutlet weak var containerCountryView: UIView!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var oldPassword: FloatLabelTextField!
    @IBOutlet weak var password: FloatLabelTextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var email: FloatLabelTextField!
    @IBOutlet weak var lastName: FloatLabelTextField!
    @IBOutlet weak var firstName: FloatLabelTextField!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var smokerSwitch: UISwitch!
  
    var paramters:[String:Any] = [:]
    var countries:[ConfigCountry] = []
    var countrySelected:Int = 0
    var viewModel:UserViewModel?
    var imagePicker:ImagePickerHelper?
    var smoker: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupProfile()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    func setup() {
        self.viewModel = UserViewModel()
        self.viewModel?.delegate = self
        
        countryImage.image = nil
        countryCode.text = "+966"
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        if let _ = BaseController.config?.countries {
            self.countries.append(contentsOf: BaseController.config!.countries!)
            self.countryPicker.reloadAllComponents()
            if countries.isset(countrySelected){
                countryImage.setImage(url: countries[countrySelected].image)
                countryCode.text = countries[countrySelected].code
            }
        }
        
    }
    func setupProfile(){
        let user = UserRoot.getUserFromCache()
        userImage.setImage(url: user.result?.driver?.car?.car_image)
        firstName.text = user.result?.first_name
        lastName.text = user.result?.last_name
        email.text = user.result?.email
        mobile.text = user.result?.mobile
        if case user.result?.smoker = true {
            smokerSwitch.isOn = true
            smoker = true
        } else {
            smokerSwitch.isOn = false
            smoker = false
        }
    }
    override func bind() {
        self.viewModel?.model.bind({ (data) in
            self.navigationController?.pop()
        })
    }
    
    override func notifySetting() {
        countryImage.image = nil
        countryCode.text = "+966"
        if let _ = BaseController.config?.countries {
            self.countries.append(contentsOf: BaseController.config!.countries!)
            self.countryPicker.reloadAllComponents()
            if countries.isset(countrySelected){
                countryImage.setImage(url: countries[countrySelected].image)
                countryCode.text = countries[countrySelected].code
            }
        }
    }
    override func validation() -> Bool {
        let validate = Validation(textFields: [firstName , lastName , email , mobile ])
        return validate.success
    }
    
}

extension EditProfile {
    @IBAction func smokerAction(_ sender: UISwitch) {
        if sender.isOn {
            self.smoker = true
        } else {
            self.smoker = false
        }
    }
    @IBAction func agreePicker(_ sender: Any) {
        if countries.isset(countrySelected){
            countryImage.setImage(url: countries[countrySelected].image)
            countryCode.text = countries[countrySelected].code
        }
        self.containerCountryView.isHidden = true
    }
    @IBAction func cancelPicker(_ sender: Any) {
        self.containerCountryView.isHidden = true
    }
    @IBAction func dropDownCountry(_ sender: Any) {
        self.containerCountryView.isHidden = false
        self.countryPicker.reloadAllComponents()
    }
    @IBAction func changeCarImage(_ sender: Any) {
        imagePicker = ImagePickerHelper(self)
    }
    @IBAction func edit(_ sender: Any) {
        let user = UserRoot.getUserFromCache()
        guard let category = user.result?.driver?.category_id else { return }
        makeAlert(translate(translate("edit_profile"))) {
            if self.validation(){
                if !self.countries.isset(self.countrySelected){
                    self.SnackBar(message: translate("please_select_country"))
                    return
                }
                self.paramters["online"] = 1
                self.paramters["first_name"] = self.firstName.text
                self.paramters["last_name"] = self.lastName.text
                self.paramters["email"] = self.email.text
                self.paramters["mobile"] = self.mobile.text
                self.paramters["country_id"] = self.countries[self.countrySelected].id?.string
                self.paramters["category_id"] = category
                if self.password.text != nil && self.password.text!.count > 1 {
                    self.paramters["old_password"] = self.oldPassword.text
                    self.paramters["password"] = self.password.text
                }
                if self.smoker != nil {
                    if case self.smoker = true {
                        self.paramters["smoker"] = "true"
                    } else {
                        self.paramters["smoker"] = "false"
                    }
                }
                
                self.viewModel?.update(paramters: self.paramters)
            }
        }
        
    }
}

extension EditProfile:UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return self.countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.countries[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.countrySelected = row
    }
    
}
extension EditProfile:ImagePickerDelegate {
    func pickerCallback(image: UIImage) {
        self.userImage.image = image
        paramters["car_image"] = image.base64(format: .JPEG(0.1))
    }
}
