//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps
class Home:BaseTabBar {
    
    @IBOutlet weak var historyCollection: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var numberOfTrips: UILabel!
    @IBOutlet weak var onlineHours: UILabel!
    @IBOutlet weak var totalEarning: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    
    var mapHelper:GoogleMapHelper?
    var viewModel:TripViewModel?
    var userViewModel:UserViewModel?
    var oldLat:Double?
    var oldLng:Double?
    var lat:Double?
    var lng:Double?
    var trip:TripResult?
    var driverMarker:GMSMarker = GMSMarker()
    var history:[TripResult] = []
    var selectedHistory:Int?
    var userCache:UserRoot?
    /** for check is refreshing loaded */
    var userLoaded:Bool = false
    /** check if pop up is on view */
    var newRequest:Bool = false
    /** stopping timer */
    var stopTimer:Bool = false
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        updateLocation()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel = TripViewModel()
        userViewModel = UserViewModel()
        bind()
        if checkIfCompany() {
            historyCollection.isHidden = false
            self.setupCompany()
        }else{
            historyCollection.isHidden = true
        }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.tripDelegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel = nil
        userViewModel = nil
        stopTimer = false
        userLoaded = false
        newRequest = false
        
        history.removeAll()
    }
    
    func checkIfCompany()-> Bool {
        let user = UserRoot.getUserFromCache()
        if let category = user.result?.driver?.category_calculating_pricing  {
            if category == "companies" || category == "truck_water" || category == "tanks" {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    func setupCompany(){
        userViewModel?.fetchHome()
        historyCollection.delegate = self
        historyCollection.dataSource = self
        historyCollection.reloadData()
    }
    override func setup() {
        
        mapHelper = GoogleMapHelper()
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapHelper?.mapView = mapView
        mapHelper?.delegate = self
        mapHelper?.drawMarkerOnLocationChanged = false
        mapHelper?.stopUpdatedLocation = true
        mapHelper?.markerDataSource = self
        mapHelper?.currentLocation()
        
        refresh()
    }
    func refresh() {
        let user = UserRoot.getUserFromCache()
        userCache = user
        if user.result?.active == "0"{
            driverStatus.isUserInteractionEnabled = false
            driverStatus.isOn = false
            statusText.text = translate("offline")
        } else {
            if user.result?.driver?.online != nil && user.result!.driver!.online! {
                driverStatus.isUserInteractionEnabled = true
                driverStatus.isOn = true
                statusText.text = translate("online")
            } else {
                driverStatus.isUserInteractionEnabled = true
                driverStatus.isOn = false
                statusText.text = translate("offline")
            }
            
        }
        
        numberOfTrips.text = "\(translate(user.result?.driver?.statistics?.today_trips?.string ?? "")) \(translate("trips"))"
        totalEarning.text = "\(translate(user.result?.driver?.statistics?.today_earned?.string ?? "") ) \(translate("SAR"))"
        onlineHours.text = "\(translate(user.result?.driver?.statistics?.online_hours?.string ?? "")) \(translate("hours_online"))"
        carImage.setImage(url: user.result?.driver?.category_icon)
        self.checkRequest(user: user)
        self.checkTrip(user: user)
    }
    func checkRequest(user: UserRoot) {
        if userLoaded {
            
            if checkIfCompany() {
                if user.result?.driver?.currentRequest?.id != nil {
                    if !newRequest {
                        newRequest = true
                        let trip = TripResult()
                        trip.from_location = user.result?.driver?.currentRequest?.trip_orgin
                        trip.to_location = user.result?.driver?.currentRequest?.trip_destination
                        trip.trip_special_description = user.result?.driver?.currentRequest?.trip_special_description
                        trip.trip_special_notes = user.result?.driver?.currentRequest?.trip_special_notes
                        trip.status = 0
                        let vc = pushViewController(NewRequestCompanyPOP.self)
                        vc.delegate = self
                        vc.request = trip
                        pushPop(vc: vc)
                    }
                }else{
                    newRequest = false
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                if user.result?.driver?.currentRequest?.id != nil {
                    if !newRequest {
                        newRequest = true
                        let vc = pushViewController(NewRequestPOP.self)
                        vc.delegate = self
                        vc.request = user.result?.driver?.currentRequest
                        pushPop(vc: vc)
                    }
                }else{
                    newRequest = false
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
       
    }
    func checkTrip(user:UserRoot){
        if userLoaded {
            if checkIfCompany() {
                userViewModel?.fetchHome()
            }else{
                if user.result?.driver?.currentTrip?.id != nil {
                    self.trip = user.result?.driver?.currentTrip
                    BaseController.currentTrip = self.trip
                    let vc = pushViewController(OnTrip.self)
                    vc.trip = self.trip
                    push(vc,false)
                }
            }
        }
        
    }
  
    override func bind() {
        userViewModel?.home.bind({ (data) in
            self.history.removeAll()
            self.history.append(contentsOf: data)
            self.historyCollection.reloadData()

        })

        userViewModel?.model.bind({ (data) in
            self.stopLoading()
            self.userLoaded = true
            self.refresh()
        })
        
        viewModel?.message.bind({ (data) in
            self.newRequest = false
        })
        viewModel?.accepting.bind({ (data) in
            if self.checkIfCompany() {
                if self.selectedHistory != nil {
                    self.history.remove(at: self.selectedHistory!)
                }
            }else{
                self.stopTimer = true
                self.newRequest = false
                self.updateUser()
            }
        })
        
        
    }
    
}

/** map functions **/
extension Home {
    func updateLocation(){
        self.updateUser()
        let _ = TimeHelper(seconds: 8, closure: updateMap)
    }
    func updateMap(counter:Int){
        if !stopTimer {
            mapHelper?.currentLocation()
        }
    }
    func updateUser(){
        self.drawMarker()
        var paramters:[String:Any] = [:]
        if self.lat != nil && self.lng != nil && userCache?.result?.active == "1" && driverStatus.isOn {
            
            paramters["lat"] = self.lat
            paramters["lng"] = self.lng
            paramters["online"] = 1

            guard let category = UserRoot.getUserFromCache().result?.driver?.category_id else { return }
            paramters["category_id"] = category
            userViewModel?.update(paramters: paramters)
        }else if self.lat != nil && self.lng != nil && userCache?.result?.active == "0" {
            paramters["lat"] = self.lat
            paramters["lng"] = self.lng
            guard let category = UserRoot.getUserFromCache().result?.driver?.category_id else { return }
            paramters["category_id"] = category
            userViewModel?.update(paramters: paramters)
        } else if userCache?.result?.active == "1" {
            guard let category = UserRoot.getUserFromCache().result?.driver?.category_id else { return }
            paramters["category_id"] = category
            paramters["online"] = 1
            userViewModel?.update(paramters: paramters)
        }
        
        
    }
    func drawMarker(){
        let imageView = UIImageView()
        if self.lat != nil && self.lng != nil {
            driverMarker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lng!)
            driverMarker.icon = #imageLiteral(resourceName: "carYellow")
            if imageView.image == nil {
                imageView.setImage(url: userCache?.result?.driver?.category_icon) {
                    self.driverMarker.icon = imageView.image?.imageResize(CGSize(width: 25, height: 25))
                }
            }
            if self.oldLat != nil && self.oldLng != nil {
                driverMarker.oldPosition = CLLocationCoordinate2D(latitude: oldLat!, longitude: oldLng!)
            }
            if driverMarker.map == nil {
                driverMarker.map = self.mapView
            }
            self.mapHelper?.refreshARMovement(marker: driverMarker)

        }
      
        
        
    }
}

extension Home:GoogleMapHelperDelegate {
    func locationCallback(lat: Double, lng: Double) {
        if oldLat == nil && oldLng == nil {
            self.oldLat = lat
            self.oldLng = lng
        }else {
            self.oldLat = self.lat
            self.oldLng = self.lng
            self.lat = lat
            self.lng = lng
        }
       
        self.updateUser()
    }
}

extension Home:MarkerDataSource {
   
    func marker() -> MarkerAttrbuite {
        var attr = MarkerAttrbuite()
        attr.use = .icon
        attr.icon = #imageLiteral(resourceName: "carYellow")
        return attr
    }
    
}

extension Home:NewRequestDelegate {
    func accept() {
        self.startLoading()
        viewModel?.accept()
    }
    
    func reject() {
        viewModel?.reject()
    }
    
    
}


extension Home:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if history[indexPath.item].status != nil && history[indexPath.item].status! < 7{
            guard var cell = tableView.cell(type: HistoryRequestCell.self, indexPath) else { return UITableViewCell() }
            cell.model = history[indexPath.item]
            cell.delegate = self
            return cell
        }else{
            guard var cell = tableView.cell(type: HistoryCell.self, indexPath) else { return UITableViewCell() }
            cell.model = history[indexPath.item]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if history[indexPath.row].status != nil && history[indexPath.row].status! == 6 {
            let vc = pushViewController(CollectCompanyRequest.self)
            vc.trip = history[indexPath.row]
            push(vc)
        }
    }

    
}

extension Home:HistoryRequestDelegate,NewRequestCompanyDelegate{
    
    func acceptCompany(price:String,path:Int? = nil) {
        viewModel?.accept(price: price)
        selectedHistory = path
    }
    
    func rejectCompany() {
        self.newRequest = false
    }
    
    func openRequest(path: Int) {
        let item = history[path]
        let vc = pushViewController(NewRequestCompanyPOP.self)
        vc.delegate = self
        vc.path = path
        vc.request = item
        pushPop(vc: vc)
    }
    func openCollect(path: Int) {
        if history[path].status != nil && history[path].status! == 6 {
            let vc = pushViewController(CollectCompanyRequest.self)
            vc.trip = history[path]
            push(vc)
        }
    }
    
    func openTo(lat: Double?, lng: Double?, address: String?) {
        let vc = pushViewController(DeliveryCompanyRequest.self)
        vc.lat = lat
        vc.lng = lng
        vc.address = address
        push(vc)
    }
    
    func openFrom(lat: Double?, lng: Double?, address: String?) {
        let vc = pushViewController(DeliveryCompanyRequest.self)
        vc.lat = lat
        vc.lng = lng
        vc.address = address
        push(vc)
    }
}


// MARK: - chat Delegate to Appdelegate
protocol TripDelegate: class {
    func didReceive(trip: TripResult?)
}
fileprivate weak var _tripDelegate: TripDelegate?
extension AppDelegate {
    weak var tripDelegate: TripDelegate? {
        set {
            _tripDelegate = newValue
        } get {
            return _tripDelegate
        }
    }
}

// MARK: - TripDelegate
extension Home: TripDelegate {
    func didReceive(trip: TripResult?) {
        guard let trip = trip else { return }
        if !newRequest {
            newRequest = true
            let vc = pushViewController(NewRequestPOP.self)
            vc.delegate = self
            let request = CurrentRequest()
            request.from_lat = trip.from_lat
            request.from_lng = trip.from_lng
            request.time_estimation = trip.time_estimation
            request.fare_estimation = trip.fare_estimation
            request.trip_orgin = trip.from_location
            request.trip_delivery_note = trip.trip_delivery_note
            
            vc.request = request
            pushPop(vc: vc)
        }
    }
}

