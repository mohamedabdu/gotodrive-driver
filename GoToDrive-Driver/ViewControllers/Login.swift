//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Login:BaseController {
    @IBOutlet weak var username: FloatLabelTextField!
    @IBOutlet weak var password: FloatLabelTextField!
    
    var viewModel:UserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      viewModel = nil
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
    }
    override func bind() {
        viewModel?.model.bind({ (data) in
            if data.result?.driver?.id == nil {
                let vc = self.pushViewController(Categories.self)
                self.push(vc)
            }else{
                let vc = self.pushViewController(Home.self)
                self.push(vc)
            }
          
        })
    }
    override func validation() -> Bool {
        //let validate = Validation(textFields: [username,password])
        return true 
        //return validate.success
    }
   
}

extension Login {
    @IBAction func google(_ sender: Any) {
        let provider = GoogleDriver()
        provider.closure = { model in
            guard let id = model.id else { return }
            print(id , model.email)
            var paramters:[String:String] = [:]
            paramters["social_id"] = id
            paramters["first_name"] = model.givenName
            paramters["last_name"] = model.familyName
            paramters["email"] = model.email
            
            self.viewModel?.social(paramters: paramters)
        }
        provider.googleProvider()
        
    }
    @IBAction func facebook(_ sender: Any) {
        let provider = FacebookDriver(delegate: self)
        provider.callback { (model) in
            guard let id = model.id else { return }
            print(id , model.email)
            var paramters:[String:String] = [:]
            paramters["social_id"] = id
            paramters["first_name"] = model.name
            paramters["email"] = model.email
            paramters["image"] = model.image
            
            self.viewModel?.social(paramters: paramters)
        }
    }
    
    @IBAction func forget(_ sender: Any) {
        let vc = pushViewController(ForgetPassword.self)
        push(vc)
    }
    @IBAction func login(_ sender: Any) {
        if self.validation() {
            viewModel?.login(username: username.text!, password: password.text!)
        }
    }
}
