//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class MakeTransaction:BaseController {
    
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var transactionBtn: UIButton!
    
    var viewModel:UserViewModel?
    var requestType:Apis = .payto_admin
    var openOnline:Bool = false
    var weeklyStatistics:HistoryStatistic?
    var fromOnlineFaild:Bool = false
    var requestID:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        balance.text = ""
        viewModel = UserViewModel()
        viewModel?.delegate = self
        guard let weekly = weeklyStatistics else { return }
        if let driverEarn = weekly.weeklyDriver , let adminEarn = weekly.weeklyAdmin {
            if driverEarn > adminEarn {
                let price = driverEarn - adminEarn
                balance.text = price.decimal(2)
                transactionBtn.setTitle(translate("request_from_go_to_drive"), for: .normal)
                requestType = .request_from_admin
            }else if adminEarn > driverEarn {
                let price = adminEarn - driverEarn
                let negAdmin = -price
                balance.text = negAdmin.decimal(2)
                transactionBtn.setTitle(translate("send_to_admin"), for: .normal)
                requestType = .payto_admin
            }else{
                return
            }
        }else{
            return
        }
    }
    
    override func bind() {
        self.viewModel?.adminRequest.bind({ (data) in
            if data.id != nil {
                if self.openOnline {
                    self.requestID = data.id
                    let vc = self.pushViewController(PaymentOnline.self)
                    vc.delegate = self
                    vc.requestID = data.id?.string
                    self.present(vc, animated: true, completion: nil)
                }else{
                    self.SnackBar(message: translate("trasnaction_successfull"), dismissClosure:{
                        self.navigationController?.pop()
                    })
                }
            }else{
                guard let message = data.message else { return }
                self.SnackBar(message: message, dismissClosure:{
                    self.navigationController?.pop()
                })
            }
           
        })
    }
    
    @IBAction func makeTransaction(_ sender: Any) {
    
        if requestType == .payto_admin {
            if fromOnlineFaild {
                let vc = self.pushViewController(PaymentOnline.self)
                vc.delegate = self
                vc.requestID = requestID?.string
                self.present(vc, animated: true, completion: nil)
                return
            }
            openOnline = true
            self.viewModel?.payToAdmin()
        }else{
            openOnline = false
            self.viewModel?.requestToAdmin()
        }
    }
    
}

extension MakeTransaction:PaymentRefreshDelegate {
    func success() {
        let vc = pushViewController(Home.self)
        push(vc)
    }
    func faild() {
        self.fromOnlineFaild = true
        self.SnackBar(message: translate("payment_faild"))
    }
}
