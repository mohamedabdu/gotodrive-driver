//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps

protocol NewRequestCompanyDelegate:class {
    func acceptCompany(price:String,path:Int?)
    func rejectCompany()
}

class NewRequestCompanyPOP:BaseController {
  
    @IBOutlet weak var fromLocation: UITextField!
    @IBOutlet weak var toLocation: UITextField!
    @IBOutlet weak var typeOfGoods: UITextField!
    @IBOutlet weak var notes: UITextField!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var typeOfGoodsLabel: UILabel!
    
    
    var path:Int?
    var request:TripResult?
    weak var delegate:NewRequestCompanyDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func setup() {
        /** check driver categories **/
        if let categoryID = UserRoot.instance.result?.driver?.category_id {
            if categoryID > 4 {
                fromLocation.isHidden = true
                typeOfGoodsLabel.text = translate("required")
            }
        }
        fromLocation.text = request?.from_location
        toLocation.text = request?.to_location
        typeOfGoods.text = request?.trip_special_description
        notes.text = request?.trip_special_notes
        if request?.status != nil && request?.status! == 0 {
            price.text = ""
            price.isUserInteractionEnabled = true
            sendBtn.isHidden = false
        }else{
            price.text = request?.total_price?.string
            price.isUserInteractionEnabled = true
            sendBtn.isHidden = true
        }
        
    }
  
    override func bind() {
        
    }
    @IBAction func accept(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.acceptCompany(price: self.price.text!,path: self.path)
        }
    }
    @IBAction func reject(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.rejectCompany()
        }
    }
}
