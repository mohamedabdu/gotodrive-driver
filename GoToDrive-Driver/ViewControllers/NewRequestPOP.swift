//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps

protocol NewRequestDelegate:class {
    func accept()
    func reject()
}

class NewRequestPOP:BaseController {
    @IBOutlet weak var timeEstimation: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var fareEstimation: UILabel!
    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var orginLocation: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var noteDeliveryRequest: UILabel!
    
    
    var request:CurrentRequest?
    weak var delegate:NewRequestDelegate?
    
    var mapHelper:GoogleMapHelper?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func setup() {
        timeEstimation.text = request?.time_estimation
        timeLabel.text = translate("min")
        fareEstimation.text = request?.fare_estimation?.double()?.decimal(2)
        fareLabel.text = translate("SAR")
        orginLocation.text = request?.trip_orgin

        /** setup note delivery */
        noteDeliveryRequest.text = "\(translate("note_:")) \(request?.trip_delivery_note ?? "")"
        setupMap()
        
    }
    func setupMap(){
        mapHelper = GoogleMapHelper()
        mapHelper?.mapView = mapView
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        mapHelper?.zoom = .city
        mapHelper?.changeCameraOnMarker = true
        //mapHelper?.currentLocation()
        mapHelper?.updateCamera(lat: request?.from_lat ?? 0, lng: request?.from_lng ?? 0)
    }
    override func bind() {
        
    }
    @IBAction func accept(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.accept()
        }
    }
    @IBAction func reject(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.reject()
        }
    }
}

extension NewRequestPOP:GoogleMapHelperDelegate,MarkerDataSource{
    func locationCallback(lat: Double, lng: Double) {
        mapHelper?.setMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))

    }
    func marker() -> MarkerAttrbuite {
        var marker = MarkerAttrbuite()
        marker.use = .icon
        marker.icon = #imageLiteral(resourceName: "pinColored")
        return marker
    }
}
