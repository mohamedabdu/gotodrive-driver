//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import GoogleMaps
import MapKit
class OnTrip:BaseController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapHeight: NSLayoutConstraint!
    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var timeArrive: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var toDestination: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var actionBtnWidth: NSLayoutConstraint!
    
    var viewModel:TripViewModel?
    var trip:TripResult?
    var mapHelper:GoogleMapHelper?
    var lat:Double?
    var lng:Double?
    var oldLat:Double?
    var oldLng:Double?
    var driverMarker:GMSMarker = GMSMarker()
    
    var timerMap:TimeHelper?
    var timerTrip:TimeHelper?
    /** options **/
    /** if map load update camera manual */
    var mapLoaded:Bool = false

    /* button Checked clicked running */
    var changeStatusRunning:Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.tripDelegate = self
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        timerMap?.stopTimer()
        timerMap = nil
        timerTrip?.stopTimer()
        timerTrip = nil
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.tripDelegate = nil
    }
    func updateLocation(){
        timerMap = TimeHelper(seconds: 4, closure: updateMap)
        timerTrip = TimeHelper(seconds: 8, closure: updateTrip)

    }
    func setup() {
        viewModel = TripViewModel()
        mapHelper = GoogleMapHelper()
        mapHelper?.mapView = mapView
        mapHelper?.drawMarkerOnLocationChanged = false
        mapHelper?.moveCameraOnUpdateLocation = false
        mapHelper?.changeCameraOnMarker = false
        mapHelper?.clearMapOnUpdateLocation = false
        mapHelper?.delegate = self
        mapHelper?.markerDataSource = self
        mapHelper?.polylineDataSource = self
        mapHelper?.currentLocation()
        
        setupClient()
        tripStatus()
        
    }
    func setupClient(){
        clientName.text = "\(trip?.client?.first_name ?? "") \(trip?.client?.last_name ?? "")"
        clientImage.setImage(url: trip?.client?.image)
        timeArrive.text = trip?.time_estimation
        distance.text = "\(trip?.distance?.string ?? "") km"
        
    }
    func updateMap(counter:Int){
        mapHelper?.currentLocation()
    }
    func updateTrip(counter:Int){
        viewModel?.current()
    }
    func tripStatus(){
        self.setupClient()
        guard let status = self.trip?.status else { return }
        switch status {
            
        case TripViewModel.TripStatus.accepted.rawValue:
            self.toDestination.text = trip?.from_location
            drawRoadToClient()
            self.actionBtn.setTitle(translate("arrived"), for: .normal)

            
        case TripViewModel.TripStatus.arrived.rawValue:
            self.toDestination.text = trip?.from_location
            removeRoute()
            self.actionBtn.setTitle(translate("start_trip"), for: .normal)
            
        case TripViewModel.TripStatus.started.rawValue:
            self.toDestination.text = trip?.to_location
            drawRoadToDestination()
            self.actionBtn.setTitle(translate("complete"), for: .normal)
            
            self.cancelBtn.isHidden = true
            self.actionBtnWidth = self.actionBtnWidth.setMultiplier(multiplier: 0.90)
            
        case TripViewModel.TripStatus.completed.rawValue:
            self.toDestination.text = trip?.to_location
            removeRoute()
            self.actionBtn.setTitle(translate("cash_collection"), for: .normal)

            
        case TripViewModel.TripStatus.collected.rawValue:
            let vc = pushViewController(CashCollection.self)
            vc.trip = self.trip
            push(vc)
            
        default:
            self.navigationController?.pop()
            break
        }
        self.statusLearnTrip()
        
    }
    func statusLearnTrip() {
        guard let status = self.trip?.status else { return }
        if status == TripViewModel.TripStatus.started.rawValue && trip?.category_calculating_price == Categories.CategoriesNames.learning.rawValue && trip?.learn_status_text == "start_trip" {
            self.actionBtn.setTitle(translate("start_class"), for: .normal)

        } else if status == TripViewModel.TripStatus.started.rawValue && trip?.category_calculating_price == Categories.CategoriesNames.learning.rawValue && trip?.learn_status_text == "start_class" {
            self.actionBtn.setTitle(translate("end_class"), for: .normal)
        }
    }
    override func bind() {
        viewModel?.currentTrip.bind({ (data) in
            self.changeStatusRunning = false
            if data.id != nil {
                self.trip = data
                BaseController.currentTrip = self.trip
                self.tripStatus()
            }else{
                self.trip = nil
                BaseController.currentTrip = self.trip
                self.backBtn(self)
            }
        })
        viewModel?.cancelMessage.bind({ (data) in
            self.trip = nil
            BaseController.currentTrip = self.trip
            self.backBtn(self)
        })
    }
    
    override func backBtn(_ sender: Any) {
        if BaseController.currentTrip?.id == nil && self.trip?.id == nil {
            self.navigationController?.pop()
        }
    }
    
    @IBAction func chageStatus(_ sender: Any) {
        guard let status = self.trip?.status else { return }
        
        if trip?.category_calculating_price == Categories.CategoriesNames.learning.rawValue {
            if status == TripViewModel.TripStatus.accepted.rawValue {
                changeStatusRunning = true
                viewModel?.arrive()
            } else if status == TripViewModel.TripStatus.arrived.rawValue {
                changeStatusRunning = true
                viewModel?.start()
            } else if status == TripViewModel.TripStatus.started.rawValue && trip?.learn_status_text == "start_trip" {
                guard let lat = self.lat , let lng = self.lng else { return }
                changeStatusRunning = true
                viewModel?.startLearn(lat: lat, lng: lng)
                
            } else if status == TripViewModel.TripStatus.started.rawValue && trip?.learn_status_text == "start_class" {
                guard let lat = self.lat , let lng = self.lng else { return }
                changeStatusRunning = true
                viewModel?.endLearn(lat: lat, lng: lng)
            } else if status == TripViewModel.TripStatus.started.rawValue && trip?.learn_status_text == "end_class" {
                guard let lat = self.lat , let lng = self.lng else { return }
                changeStatusRunning = true
                viewModel?.complete(lat: lat, lng: lng)
            }
        } else {
            if status == TripViewModel.TripStatus.accepted.rawValue {
                changeStatusRunning = true
                viewModel?.arrive()
            } else if status == TripViewModel.TripStatus.arrived.rawValue {
                changeStatusRunning = true
                viewModel?.start()
            } else if status == TripViewModel.TripStatus.started.rawValue {
                guard let lat = self.lat , let lng = self.lng else { return }
                changeStatusRunning = true
                viewModel?.complete(lat: lat, lng: lng)
            }
        }
       
        if status == TripViewModel.TripStatus.completed.rawValue {
            if  let paymentPaid = self.trip?.payment_paid , let paymentMethod = self.trip?.payment_method {
                if paymentMethod == 2 {
                    if !paymentPaid {
                        self.SnackBar(message: translate("payment_faild"), duration: .short)
                        return
                    }
                }
                
            }
            let vc = pushViewController(CashCollection.self)
            vc.trip = self.trip
            push(vc)
        }
    }
    @IBAction func chat(_ sender: Any) {
        let vc = pushViewController(Chat.self)
        vc.tripID = trip?.id
        vc.friendID = trip?.client?.id
        push(vc)
    }
    @IBAction func cancelTrip(_ sender: Any) {
        let vc = pushViewController(BookingCancelPOP.self)
        vc.delegate = self
        pushPop(vc: vc)
    }
    @IBAction func deliverMe(_ sender: Any) {
        openMapForPlace()
    }
    @IBAction func callClient(_ sender: Any) {
        call(text: self.trip?.client?.mobile)
    }
    
    func getDeliveryOption()->(Double,Double,String?){
        guard let status = self.trip?.status else { return (0 , 0 ,"")  }
        if status == TripViewModel.TripStatus.accepted.rawValue {
            if let lat = self.trip?.from_lat , let lng = self.trip?.from_lng {
                return ( lat , lng , self.trip?.from_location )
            }
        }else if status == TripViewModel.TripStatus.started.rawValue {
            if let lat = self.trip?.to_lat , let lng = self.trip?.to_lng {
                return ( lat , lng , self.trip?.to_location )
            }
        }else if status == TripViewModel.TripStatus.completed.rawValue {
            if let lat = self.trip?.to_lat , let lng = self.trip?.to_lng {
                return ( lat , lng , self.trip?.to_location )
            }
        }
        return (0 , 0 ,"")
    }
    func openMapForPlace() {

        let installedNavigationApps : [String] = ["Apple Maps", "Google Maps", "Cancel"] // Apple Maps is always installed
    
        let alert = UIAlertController(title: "Selection", message: "Select Navigation App", preferredStyle: .actionSheet)
        for app in installedNavigationApps {
            let button = UIAlertAction(title: app, style: .default){ (action) in
                if action.title == "Apple Maps" {
                    self.openAppleMap()
                }else if action.title == "Google Maps" {
                    self.openGoogleMap()
                }
            }
            
            alert.addAction(button)
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    func openAppleMap(){
        let (lat, lng, title) = getDeliveryOption()
        
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = lng
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = title
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    func openGoogleMap(){
        let (lat, lng, _) = getDeliveryOption()
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:
                "comgooglemaps://?saddr=&daddr=\(lat),\(lng)&directionsmode=driving")!, options: [:], completionHandler: nil)
        } else {
            NSLog("Can't use comgooglemaps://");
        }
    }
    
}

extension OnTrip:GoogleMapHelperDelegate{
    func locationCallback(lat: Double, lng: Double) {
        if oldLat == nil && oldLng == nil {
            self.oldLat = lat
            self.oldLng = lng
        }else {
            self.oldLat = self.lat
            self.oldLng = self.lng
            self.lat = lat
            self.lng = lng
        }
        if !mapLoaded {
            mapLoaded = true
            self.mapHelper?.updateCamera(lat: lat, lng: lng)
        }
        refreshMarkerMovement()
        tripStatus()
    }
    func refreshMarkerMovement(){
        if self.lat != nil && self.lng != nil {
            let imageView = UIImageView()
            driverMarker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lng!)
            driverMarker.icon = #imageLiteral(resourceName: "carYellow")
            if imageView.image == nil {
                let icon = UserRoot.getUserFromCache().result?.driver?.category_icon
                imageView.setImage(url: icon) {
                    self.driverMarker.icon = imageView.image?.imageResize(CGSize(width: 25, height: 25))
                }
            }
            if self.oldLat != nil && self.oldLng != nil {
                driverMarker.oldPosition = CLLocationCoordinate2D(latitude: oldLat!, longitude: oldLng!)
            }
            if driverMarker.map == nil {
                driverMarker.map = self.mapView
            }
            self.mapHelper?.refreshARMovement(marker: driverMarker)
            
        }
    }
}

/** dataSource of Map */
extension OnTrip:MarkerDataSource,PolylineDataSource {
    func marker() -> MarkerAttrbuite {
        var marker = MarkerAttrbuite()
        marker.use = .icon
        marker.icon = #imageLiteral(resourceName: "pinColored")
        return marker
    }
    func polyline() -> PolylineAttrbuite {
        var poly = PolylineAttrbuite()
        poly.width = 2
        poly.color = .blue
        return poly
    }
    
}
/** accepted trip functions */
extension OnTrip{
    func drawRoadToClient(){
        if self.lat == nil && self.lng == nil {
            return
        }
        guard let lat = trip?.from_lat , let lng = trip?.from_lng else { return }
        let orgin = CLLocation(latitude: self.lat!, longitude: self.lng!)
        let destination = CLLocation(latitude: lat, longitude: lng)
        /** set marker of client **/
        self.mapHelper?.setMarker(position: destination.coordinate)
        
        self.mapHelper?.drawRoute(orgin: orgin, destination: destination)
    }
}

/** arrive trip functions */
extension OnTrip{
    func removeRoute(){
        if self.mapHelper?.polyline != nil {
            self.mapHelper?.polyline.map = nil
        }

    }
}
/** started trip functions */
extension OnTrip{
    func drawRoadToDestination(){
        if self.lat == nil && self.lng == nil {
            return
        }
        guard let lat = trip?.from_lat , let lng = trip?.from_lng else { return }
        let orgin = CLLocation(latitude: lat, longitude: lng)
        guard let toLat = trip?.to_lat , let toLng = trip?.to_lng else { return }
        let destination = CLLocation(latitude: toLat, longitude: toLng)
        /** set marker of client **/
        self.mapHelper?.setMarker(position: destination.coordinate)
        self.mapHelper?.drawRoute(orgin: orgin, destination: destination)
    }
}

extension OnTrip: BookingCancelDelegate, CancelReasonDelegate {
    func apply() {
        let vc = pushViewController(CancelationReasonPOP.self)
        vc.delegate = self
        pushPop(vc: vc)
    }
    func reason(reason: Int) {
        self.viewModel?.delegate = self
        self.viewModel?.cancel(reason: reason)
    }
}

// MARK: - TripDelegate
extension OnTrip: TripDelegate {
    func didReceive(trip: TripResult?) {
        if let trip = trip {
            self.trip = trip
            BaseController.currentTrip = trip
            self.tripStatus()
        }
        
    }
}
