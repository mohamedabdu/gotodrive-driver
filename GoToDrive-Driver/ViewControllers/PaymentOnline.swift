//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import WebKit
import UIKit


protocol PaymentRefreshDelegate:class {
    func faild()
    func success()
    func closePayment()
}

extension PaymentRefreshDelegate {
    func closePayment(){
        
    }
    func faild(){
        
    }
    func success(){
        
    }
}
class PaymentOnline:BaseController, WKUIDelegate , PaymentRefreshDelegate {
    
    static var tripURL = "http://nashmit.com/payment/transaction/process?trip_id="
    static var requestURL = "http://nashmit.com/payment/transaction/requestAdmin/process?request_id="

    static var paymentSuccess:Bool = false
    var requestID:String?
    weak var delegate:PaymentRefreshDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.paymentDelegate = nil
    }
    
    func setup() {
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.paymentDelegate = self
        let webView = WKWebView(frame: CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: self.view.frame.size.height))
        let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        backBtn.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
        backBtn.addTarget(self, action:#selector(cancel), for: .touchUpInside)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.30)
        self.view.addSubview(backBtn)
        self.view.addSubview(webView)
        let paymentURL = "\(PaymentOnline.requestURL)\(requestID ?? "")"
        let url = URL(string: paymentURL)
        webView.load(URLRequest(url: url!))
    }
    
    override func bind() {
        
    }
    
    @objc func cancel(){
        self.closePayment()
    }
    func closePayment(){
        if PaymentOnline.paymentSuccess {
            self.dismiss(animated: true, completion: {
                self.delegate?.success()
            })
        }else{
            self.dismiss(animated: true, completion: {
                self.delegate?.faild()
            })
        }
    }
}
