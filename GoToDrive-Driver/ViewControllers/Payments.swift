//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Payments:BaseController {
    
    @IBOutlet weak var weekEraning: UILabel!
    
    var viewModel:UserViewModel?
    var history:HistoryResult?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
        viewModel?.fetchHistory()
    }
    
    override func bind() {
        viewModel?.history.bind({ (data) in
            self.history = data
            self.weekEraning.text = self.history?.week?.statistics?.total_earned?.decimal(2)
        })
    }
    @IBAction func makeTransaction(_ sender: Any) {
        let vc = pushViewController(MakeTransaction.self)
        vc.weeklyStatistics = self.history?.week?.statistics
        push(vc)
    }
    @IBAction func transactions(_ sender: Any) {
        let vc = pushViewController(Transactions.self)
        push(vc)
    }
    @IBAction func paymentDetails(_ sender: Any) {
        let vc = pushViewController(Earning.self)
        push(vc)
    }
}
