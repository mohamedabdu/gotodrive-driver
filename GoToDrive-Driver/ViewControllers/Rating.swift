//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import Cosmos
class Rating:BaseTabBar {
    @IBOutlet weak var rateNumber: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var rateCount: UILabel!
    @IBOutlet weak var progress5: UIProgressView!
    @IBOutlet weak var progress5Count: UILabel!
    @IBOutlet weak var progress4: UIProgressView!
    @IBOutlet weak var progress4Count: UILabel!
    @IBOutlet weak var progress3: UIProgressView!
    @IBOutlet weak var progress3Count: UILabel!
    @IBOutlet weak var progress2: UIProgressView!
    @IBOutlet weak var progress2Count: UILabel!
    @IBOutlet weak var progress1: UIProgressView!
    @IBOutlet weak var progress1Count: UILabel!
    @IBOutlet weak var ratesCollection: UITableView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var viewModel:UserViewModel?
    var reviews:[ReviewRate] = []
    var reviewModel:ReviewResult?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.currentIndex = .rate
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        self.reviews.removeAll()
    }
    
    override func setup() {
        ratesCollection.delegate = self
        ratesCollection.dataSource = self
        ratesCollection.isScrollEnabled = false
        scrollView.delegate = self
        viewModel = UserViewModel()
        viewModel?.delegate = self
        viewModel?.fetchReview()
    }
    func refresh(){
        self.ratesCollection.reloadData()
        rateNumber.text = reviewModel?.avg_rate?.double.decimal(1)
        guard let rating = reviewModel?.avg_rate?.double else { return }
        rateView.rating = rating
        rateCount.text = translate(reviewModel?.count_rates?.string, "total")
        
        guard let totalRate = reviewModel?.count_rates else { return }
        
        guard let star5 = reviewModel?.statistics_text?.star5 else { return }
        progress5.progress = (star5/totalRate).float
        progress5Count.text = star5.string
        
        guard let star4 = reviewModel?.statistics_text?.star4 else { return }
        progress4.progress = (star4/totalRate).float
        progress4Count.text = star4.string
        
        guard let star3 = reviewModel?.statistics_text?.star3 else { return }
        progress3.progress = (star3/totalRate).float
        progress3Count.text = star3.string
        
        guard let star2 = reviewModel?.statistics_text?.star2 else { return }
        progress2.progress = (star2/totalRate).float
        progress2Count.text = star2.string
        
        guard let star1 = reviewModel?.statistics_text?.star1 else { return }
        progress1.progress = (star1/totalRate).float
        progress1Count.text = star1.string

        
    }
    override func bind() {
        viewModel?.reviews.bind({ (data) in
            self.reviewModel = data
            guard let list = self.reviewModel?.rates else { return }
            self.reviews.append(contentsOf: list)
            self.refresh()
        })
    }
}

extension Rating:UITableViewDelegate , UITableViewDataSource , UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.didEndDragging() && viewModel!.runPaginator() {
            viewModel?.fetchReview()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        collectionHeight.constant = (reviews.count*120).cgFloat
        return reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: RateCell.self, indexPath) else { return UITableViewCell() }
        cell.model = reviews[indexPath.row]
        return cell
    }
    
    
}
