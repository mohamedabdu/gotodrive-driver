//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Register:BaseController {
    @IBOutlet weak var countryPicker: UIPickerView!
    @IBOutlet weak var containerCountryView: UIView!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var password: FloatLabelTextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var email: FloatLabelTextField!
    @IBOutlet weak var lastName: FloatLabelTextField!
    @IBOutlet weak var firstName: FloatLabelTextField!
    @IBOutlet weak var buttonPolicy: RadioButton!
    @IBOutlet weak var birthDate: FloatLabelTextField! { didSet { birthDate.delegate = self } }
    @IBOutlet weak var femaleBtn: RadioButton!
    @IBOutlet weak var maleBtn: RadioButton!
    @IBOutlet weak var hasCarSwitch: UISwitch!
    @IBOutlet weak var smokerSwitch: UISwitch!
    
    private lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker.init()
        picker.datePickerMode = .date
        picker.maximumDate = .init()
        picker.addTarget(self, action: #selector(getDateOnDatePickerValueChanged(_:)), for: .valueChanged)
        return picker
    }()
    
    var countries:[ConfigCountry] = []
    var countrySelected:Int = 0
    var viewModel:UserViewModel?
    var bindSocial:Bool = false
    var policySelected: Bool = false
    var smoker: Bool = false
    var gender: String = "male"
    var hasCar: Bool = false
    static var hasCar: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
      viewModel = nil
    }
    
    func setup() {
        self.viewModel = UserViewModel()
        self.viewModel?.delegate = self
        
        countryImage.image = nil
        countryCode.text = "+966"
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        if let _ = BaseController.config?.countries {
            self.countries.append(contentsOf: BaseController.config!.countries!)
            self.countryPicker.reloadAllComponents()
            if countries.isset(countrySelected){
                countryImage.setImage(url: countries[countrySelected].image)
                countryCode.text = countries[countrySelected].code
            }
        }
        buttonPolicy.onSelect { [weak self] in
            self?.policySelected = true
        }
        buttonPolicy.onDeselect { [weak self] in
            self?.policySelected = false
        }
        maleBtn.onSelect { [weak self] in
            self?.gender = "male"
            self?.femaleBtn.deselect()
        }
        maleBtn.onDeselect { [weak self] in
            if self?.gender == "male" {
                self?.gender = "male"
            }
        }
        femaleBtn.onSelect { [weak self] in
            self?.gender = "female"
            self?.maleBtn.deselect()
        }
        femaleBtn.onDeselect { [weak self] in
            if self?.gender == "female" {
                self?.gender = "male"
            }
        }
    }
    override func bind() {
        self.viewModel?.model.bind({ (data) in
            if self.bindSocial {
                if data.result?.driver?.id == nil {
                    let vc = self.pushViewController(Categories.self)
                    self.push(vc)
                }else{
                    let vc = self.pushViewController(Home.self)
                    self.push(vc)
                }
            }else{
                let vc = self.pushViewController(VerifyMobile.self)
                let mobile = "\(self.countries[self.countrySelected].code ?? "")\(self.mobile.text ?? "")"
                vc.mobile = mobile
                vc.code = data.result?.activation_code
                self.push(vc)
            }
          
        })
    }
    
    override func notifySetting() {
        countryImage.image = nil
        countryCode.text = "+966"
        if let _ = BaseController.config?.countries {
            self.countries.append(contentsOf: BaseController.config!.countries!)
            self.countryPicker.reloadAllComponents()
            if countries.isset(countrySelected){
                countryImage.setImage(url: countries[countrySelected].image)
                countryCode.text = countries[countrySelected].code
            }
        }
    }
    override func validation()->Bool {
        let validate = Validation(textFields: [firstName , lastName , email , mobile , password])
        return validate.success
    }
    
}

// MARK: - date of birth picker
extension Register: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case birthDate:
            textField.inputView = datePicker
            setToolbar(for: textField)
        default:
            break
        }
    }
    @objc func getDateOnDatePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        birthDate.text = dateFormatter.string(from: sender.date)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func setToolbar(for textField: UITextField) {
        let toolbar = UIToolbar.init(frame: .init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        toolbar.tintColor = .black
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.items = [
            .init(barButtonSystemItem: .flexibleSpace, target: .none, action: .none),
            .init(title: translate("done"),
                  style: .plain,
                  target: self,
                  action: #selector(dismissKeyboard))
        ]
        textField.inputAccessoryView = toolbar
    }
}
extension Register {
    @IBAction func smokerAction(_ sender: UISwitch) {
        if sender.isOn {
            self.smoker = true
        } else {
            self.smoker = false
        }
    }
    @IBAction func hasCarAction(_ sender: UISwitch) {
        if sender.isOn {
            self.hasCar = true
        } else {
            self.hasCar = false
        }
    }
    @IBAction func acceptPolicyAction(_ sender: Any) {
        let vc = pushViewController(Help.self)
        push(vc)
    }
    @IBAction func agreePicker(_ sender: Any) {
        if countries.isset(countrySelected){
            countryImage.setImage(url: countries[countrySelected].image)
            countryCode.text = countries[countrySelected].code
        }
        self.containerCountryView.isHidden = true
    }
    @IBAction func cancelPicker(_ sender: Any) {
        self.containerCountryView.isHidden = true
    }
    @IBAction func dropDownCountry(_ sender: Any) {
        self.containerCountryView.isHidden = false
        self.countryPicker.reloadAllComponents()
    }
    @IBAction func google(_ sender: Any) {
        let provider = GoogleDriver()
        provider.closure = { model in
            guard let id = model.id else { return }
            print(id , model.email)
            var paramters:[String:String] = [:]
            paramters["social_id"] = id
            paramters["first_name"] = model.givenName
            paramters["last_name"] = model.familyName
            paramters["email"] = model.email
            self.bindSocial = true
            self.viewModel?.social(paramters: paramters)
        }
        provider.googleProvider()
        
    }
    @IBAction func facebook(_ sender: Any) {
        let provider = FacebookDriver(delegate: self)
        provider.callback { (model) in
            guard let id = model.id else { return }
            print(id , model.email)
            var paramters:[String:String] = [:]
            paramters["social_id"] = id
            paramters["first_name"] = model.name
            paramters["email"] = model.email
            paramters["image"] = model.image
            self.bindSocial = true
            self.viewModel?.social(paramters: paramters)
        }
    }
    @IBAction func next(_ sender: Any) {
        UserRoot.removeCacheingDefault()
        
        if self.validation(){
            if !countries.isset(countrySelected){
                self.SnackBar(message: translate("please_select_country"))
                return
            }
            var paramters:[String:String] = [:]
            paramters["first_name"] = firstName.text
            paramters["last_name"] = lastName.text
            paramters["email"] = email.text
            paramters["mobile"] = mobile.text
            paramters["password"] = password.text
            paramters["country_id"] = countries[countrySelected].id?.string
            paramters["date_of_birth"] = birthDate.text
            paramters["gender"] = gender
            if smoker {
                paramters["smoker"] = "true"
            } else {
                paramters["smoker"] = "false"
            }
            if hasCar {
                paramters["has_car"] = "true"
            } else {
                paramters["has_car"] = "false"
            }
            Register.hasCar = hasCar
            self.bindSocial = false
            self.viewModel?.register(paramters: paramters)
        }
    }
}

extension Register:UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return self.countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.countries[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.countrySelected = row
    }
    
}
