//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class RegisterStep2:BaseController {

    enum PickerSelection {
        case brand
        case model
        case year
        case color
    }
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var brand: FloatLabelTextField!
    @IBOutlet weak var model: FloatLabelTextField!
    @IBOutlet weak var year: FloatLabelTextField!
    @IBOutlet weak var color: FloatLabelTextField!
    @IBOutlet weak var containerViewPicker: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var transmissionView: UIView!
    
    @IBOutlet weak var manualBtn: RadioButton!
    @IBOutlet weak var automaticBtn: RadioButton!
    var brands:[ConfigBrand] = []
    var models:[ModelsResult] = []
    var years:[Int] = []
    var colors:[ConfigColor] = []
    var brandSelected:Int = 0
    var modelSelected:Int = 0
    var yearSelected:Int = 0
    var colorSelected:Int = 0
    
    static var categoryID:Int!
    static var subCategoryID:Int!
    var category:Int!
    var subCategory:Int?
    var currentSelection:PickerSelection = .brand
    var viewModel:UserViewModel?
    var settingViewModel:SettingViewModel?
    var categoryConfig: ConfigCategory?
    
    
    var imagePicker:ImagePickerHelper?
    var paramters:[String:Any] = [:]
    override func viewDidLoad() {
        super.configCategoryId = self.category
        super.configSubCategoryId = self.subCategory
        BaseController.configLoaded = false
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        settingViewModel = nil
    }
    
    func setup() {
        viewModel = UserViewModel()
        settingViewModel = SettingViewModel()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        DispatchQueue.global(qos: .background).async {
            self.years.removeAll()
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            let firstYear = 1900
            let counter = year-firstYear
            for i in 0..<counter+1   {
                self.years.append(firstYear+i)
            }
            self.years.reverse()
        }
        
        if let list = BaseController.config?.colors {
            self.colors.removeAll()
            self.colors.append(contentsOf: list)
        }
        
        if categoryConfig?.calculating_pricing == Categories.CategoriesNames.learning.rawValue {
            transmissionView.isHidden = false
            self.paramters["car_transmission"] = "automatic"
        } else {
            transmissionView.isHidden = true
        }
        
        automaticBtn.onSelect {
            self.paramters["car_transmission"] = "automatic"
            self.manualBtn.deselect()
        }
        manualBtn.onSelect {
            self.paramters["car_transmission"] = "manual"
            self.automaticBtn.deselect()

        }
    }
    override func bind() {
        settingViewModel?.models.bind({ (data) in
            self.models.removeAll()
            self.models.append(contentsOf: data)
            self.pickerView.reloadAllComponents()
        })
        
    }
    override func notifySetting() {
        self.baseViewModel?.delegate = nil
        if let list = BaseController.config?.brands {
            self.brands.removeAll()
            self.brands.append(contentsOf: list)
            self.pickerView.reloadAllComponents()
        }
    }
    override func validation() -> Bool {
        if !brands.isset(brandSelected) {
            self.SnackBar(message: translate("please_select_brand"),duration: .short)
            return false
        }
        else if !models.isset(modelSelected) {
            self.SnackBar(message: translate("please_select_model"),duration: .short)
            return false
        }
        else if !years.isset(yearSelected) {
            self.SnackBar(message: translate("please_select_year"),duration: .short)
            return false
        }
        else if !colors.isset(colorSelected) {
            self.SnackBar(message: translate("please_select_color"),duration: .short)
            return false
        }
        else if paramters["car_image"] == nil {
            self.SnackBar(message: translate("please_pick_up_a_photo"),duration: .short)
            return false
        }
        
        let validate = Validation(textFields: [model,brand,year,color])
        return validate.success
    }
   
}

extension RegisterStep2 {
    @IBAction func agreePicker(_ sender: Any) {
        self.containerViewPicker.isHidden = true
        switch currentSelection {
        case .brand:
            if brands.isset(brandSelected) {
                brand.text = brands[brandSelected].name
            }
        case .model:
            if models.isset(modelSelected) {
                model.text = models[modelSelected].name
            }
        case .year:
            if years.isset(yearSelected) {
                year.text = years[yearSelected].string
            }
        case .color:
            if colors.isset(colorSelected) {
                color.text = colors[colorSelected].name
            }
        }
        
    }
    @IBAction func cancelPicker(_ sender: Any) {
        self.containerViewPicker.isHidden = true
        
    }
    @IBAction func colorPicker(_ sender: Any) {
        self.color.becomeFirstResponder()
        self.color.text = " "
        self.color.isUserInteractionEnabled = false
        
        self.containerViewPicker.isHidden = false
        self.currentSelection = .color
        self.pickerView.reloadAllComponents()
        
        
        
    }
    @IBAction func yearPicker(_ sender: Any) {
        self.year.becomeFirstResponder()
        self.year.text = " "
        self.year.isUserInteractionEnabled = false
        
        self.containerViewPicker.isHidden = false
        self.currentSelection = .year
        self.pickerView.reloadAllComponents()
        
        
    }
    @IBAction func modelPicker(_ sender: Any) {
        if brands.isset(brandSelected) {
            self.model.becomeFirstResponder()
            self.model.text = " "
            self.model.isUserInteractionEnabled = false
            
            self.containerViewPicker.isHidden = false
            self.currentSelection = .model
            guard let id = brands[brandSelected].id else { return }
            self.settingViewModel?.brandModels(model: id)
        }
        
    }
    @IBAction func brandPicker(_ sender: Any) {
        self.brand.becomeFirstResponder()
        self.brand.text = " "
        self.brand.isUserInteractionEnabled = false
        
        self.containerViewPicker.isHidden = false
        self.currentSelection = .brand
        BaseController.configLoaded = false
        self.baseViewModel?.delegate = self
        self.setupBase()
        
    }
    @IBAction func changeImage(_ sender: Any) {
        imagePicker = ImagePickerHelper(self)
    }
    
    @IBAction func next(_ sender: Any) {
        if validation() {
            self.paramters["category_id"] = category
            if self.subCategory != nil {
                self.paramters["sub_category_id"] = subCategory!
            }
            self.paramters["brand_id"] = brands[brandSelected].id!
            self.paramters["model_id"] = models[modelSelected].id!
            self.paramters["year"] = years[yearSelected]
            self.paramters["color_id"] = colors[colorSelected].id!
            let vc = pushViewController(UploadDocument.self)
            vc.paramters = self.paramters
            push(vc)
            
        }
    }
}

extension RegisterStep2:UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch currentSelection {
        case .brand:
            return self.brands.count
        case .model:
            return self.models.count
        case .year:
            return self.years.count
        case .color:
            return self.colors.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch currentSelection {
        case .brand:
            return self.brands[row].name
        case .model:
            return self.models[row].name
        case .year:
            return self.years[row].string
        case .color:
            return self.colors[row].name
            
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch currentSelection {
        case .brand:
            brandSelected = row
        case .model:
            modelSelected = row
        case .year:
            yearSelected = row
        case .color:
            colorSelected = row
            
        }
    }
    
}

extension RegisterStep2:ImagePickerDelegate {
    func pickerCallback(image: UIImage) {
        self.carImage.image = image
        self.paramters["car_image"] = image.base64(format: .JPEG(0.075))
    }
}
