//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class ResetPassword:BaseController {
    
    @IBOutlet weak var confirmationPassword: FloatLabelTextField!
    @IBOutlet weak var newPassword: FloatLabelTextField!
    @IBOutlet weak var code: FloatLabelTextField!
    
    var email:String?
    var viewModel:UserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
    }
    override func bind() {
        viewModel?.message.bind { (data) in
            self.SnackBar(message: data,duration: .short, dismissClosure: {
                let vc = self.pushViewController(Intro.self)
                self.push(vc)
            })
        }
    }
    override func validation() -> Bool {
        confirmationPassword.customValidationRules = [RequiredRule() , ConfirmationRule(confirmField: newPassword)]
        let validate = Validation(textFields: [code,newPassword,confirmationPassword])
        return validate.success
    }
    @IBAction func reset(_ sender: Any) {
        if validation() {
            var paramters:[String:Any] = [:]
            paramters["email"] = email!
            paramters["password"] = newPassword.text!
            paramters["password_confirmation"] = confirmationPassword.text!
            paramters["token"] = code.text!
            viewModel?.reset(paramaters: paramters)
        }
    }
    
}
