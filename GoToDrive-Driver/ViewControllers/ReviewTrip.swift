//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation
import Cosmos

class ReviewTrip:BaseController {
    @IBOutlet weak var totalDistance: UILabel!
    @IBOutlet weak var totalFare: UILabel!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var comment: FloatLabelTextField!
    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var timeArrive: UILabel!
    @IBOutlet weak var distance: UILabel!
    
    var trip:TripResult?
    var viewModel:TripViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        setup()
        setupTrip()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
    }
    
    func setup() {
        viewModel = TripViewModel()
        rate.rating = 0
        totalFare.text = translate("0","SAR")
        totalDistance.text = translate("0", "km")
    }
    func setupTrip(){
        totalFare.text = translate(trip?.total_price?.string,"SAR")
        totalDistance.text = translate(trip?.distance?.string, "km")
        clientName.text = "\(trip?.client?.first_name ?? "") \(trip?.client?.last_name ?? "")"
        clientImage.setImage(url: trip?.client?.image)
        timeArrive.text = trip?.time_estimation
        distance.text = "\(trip?.distance?.string ?? "") km"
        
    }
    override func bind() {
        viewModel?.message.bind({ (message) in
        })
    }
    
    override func backBtn(_ sender: Any) {
        BaseController.currentTrip = nil
        let vc = pushViewController(Home.self)
        push(vc,false)
    }
    @IBAction func help(_ sender: Any) {
        
    }
    
    @IBAction func rateNow(_ sender: Any) {
        if rate.rating != 0 {
            guard let id = self.trip?.id else { return }
            self.viewModel?.rate(trip:id,rate: rate.rating.int, comment: comment.text)
            self.backBtn(self)
        }
    }
}
