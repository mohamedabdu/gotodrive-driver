//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Settings:BaseController {
    @IBOutlet weak var smokerSwitch: UISwitch!
    @IBOutlet weak var sameGenderSwitch: UISwitch!
    
    var viewModel:UserViewModel?
    var paramters:[String:String] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func setup() {
        self.viewModel = UserViewModel()
        self.viewModel?.delegate = self
        
        let user = UserRoot.getUserFromCache()
        
        if case user.result?.request_smoker = true {
            smokerSwitch.isOn = true
        } else {
            smokerSwitch.isOn = false
        }
        if case user.result?.request_gender = true {
            sameGenderSwitch.isOn = true
        } else {
            sameGenderSwitch.isOn = false
        }
    }
    override func bind() {
        viewModel?.model.bind({ (data) in
            
        })
    }
    
    func updateProfile() {
        self.viewModel?.update(paramters: paramters)
    }
    @IBAction func lang(_ sender: Any) {
        changeLang {
            let story = UIStoryboard(name: Storyboards.main.rawValue, bundle: nil)
            guard let vc = story.instantiateInitialViewController() else { return }
            let appdelegate = UIApplication.shared.delegate as? AppDelegate
            appdelegate?.window?.rootViewController = vc
        }
    }
    
    @IBAction func sameGenderAction(_ sender: UISwitch) {
        if sender.isOn {
            self.paramters["request_gender"]  = "true"
            self.updateProfile()
        } else {
            self.paramters["request_gender"]  = "false"
            self.updateProfile()
        }
    }
    @IBAction func smokerAction(_ sender: UISwitch) {
        if sender.isOn {
            self.paramters["request_smoker"]  = "true"
            self.updateProfile()
        } else {
            self.paramters["request_smoker"]  = "false"
            self.updateProfile()
        }
    }
}
