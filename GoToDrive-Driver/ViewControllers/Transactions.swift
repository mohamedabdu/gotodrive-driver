//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class Transactions:BaseController {
    
    @IBOutlet weak var transactionsCollection: UITableView!
    
    var transactions:[AdminRequestResult] = []
    var viewModel:UserViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        bind()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        viewModel = nil
        transactions.removeAll()
    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
        viewModel?.fetchTransactions()
        
        transactionsCollection.delegate = self
        transactionsCollection.dataSource = self
    }
    
    override func bind() {
        viewModel?.transactions.bind({ (data) in
            self.transactions.append(contentsOf: data)
            self.transactionsCollection.reloadData()
        })
    }
}

extension Transactions:UITableViewDelegate , UITableViewDataSource {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.didEndDragging() && viewModel!.runPaginator() {
            viewModel?.fetchTransactions()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: TransactionCell.self, indexPath) else { return UITableViewCell () }
        cell.model = transactions[indexPath.row]
        return cell
    }
    
    
}
