//
//  Intro.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/9/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import Foundation

class UploadDocument: BaseController {
    
    @IBOutlet weak var attachmentsCollection: UITableView! {
        didSet {
            attachmentsCollection.delegate = self
            attachmentsCollection.dataSource = self
        }
    }
    
    var paramters:[String:Any] = [:]
    var categoryID: Int?
    var viewModel: UserViewModel?
    var imagePicker: ImagePickerHelper?
    var attachments: [CategoryAttachmentResult] = []
    var selectedPath: Int?
    var selectedImage: UIImage?
    var selectedAttachment: [CategoryAttachmentResult] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let id = self.paramters["category_id"] as? Int else { return }
        categoryID = id
        setup()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {

    }
    
    func setup() {
        viewModel = UserViewModel()
        viewModel?.delegate = self
        imagePicker = ImagePickerHelper(self , run: false)
        let driverLic = CategoryAttachmentResult()
        driverLic.name = translate("driver_license")
        driverLic.key = "driving_license_img"
        attachments.append(driverLic)
        if Register.hasCar {
            let carLic = CategoryAttachmentResult()
            carLic.name = translate("car_license")
            carLic.key = "car_license_img"
            
            let insurance = CategoryAttachmentResult()
            insurance.name = translate("insurance")
            insurance.key = "insurance"
            attachments.append(carLic)
            attachments.append(insurance)

        }
    }
    override func bind() {
        viewModel?.model.bind({ (data) in
            let vc = self.pushViewController(Home.self)
            print(data)
            self.push(vc)
        })
    }
    override func validation() -> Bool {
        return true
    }
    
    @IBAction func next(_ sender: Any) {
        if validation() {
            attachments.forEach { (data) in
                if let key = data.key, let img = data.img {
                    self.paramters[key] = img
                }
            }
            self.viewModel?.registerStep2(paramters: paramters)
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension UploadDocument: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attachments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard var cell = tableView.cell(type: DocumentCell.self, indexPath) else { return UITableViewCell() }
        if indexPath.row == selectedPath {
            cell.newImage = selectedImage
        }
        cell.delegate = self
        cell.model = attachments[indexPath.row]
        return cell
    }
    
}

extension UploadDocument: DocumentCellDelegate {
    func openPickerAtPath(for path: Int) {
        selectedPath = path
        imagePicker?.openPicker()
    }
}

extension UploadDocument:ImagePickerDelegate {
    func pickerCallback(image: UIImage) {
        if selectedPath != nil {
            selectedImage = image
            if attachments[safe: selectedPath!] != nil {
                attachments[selectedPath!].img = image.base64(format: .JPEG(0.075))
                attachmentsCollection.reloadData()
            }
            
        }
      
    }
}
