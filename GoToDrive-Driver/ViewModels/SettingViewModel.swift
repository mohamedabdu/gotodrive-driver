
import Foundation

class SettingViewModel:ViewModelCore {
    
    
    var setting:DynamicType = DynamicType<ConfigResult>()
    var models:DynamicType = DynamicType<[ModelsResult]>()
    var attachments:DynamicType = DynamicType<[CategoryAttachmentResult]>()
    
    func fetchData(category:Int? = nil,subCategory:Int? = nil) {
        if category != nil {
            ApiManager.instance.paramaters["category_id"] = category
        }
        if subCategory != nil {
            ApiManager.instance.paramaters["sub_category_id"] = subCategory
        }
        delegate?.startLoading()
        ApiManager.instance.connection(.configs, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = ConfigModel.convertToModel(response: response)
            if(data.result != nil){
                self.setting.value = data.result!
            }
        }
    }
    func brandModels(model:Int) {
        
        delegate?.startLoading()
        let method = api(.models,[model])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = Models.convertToModel(response: response)
            if(data.result != nil){
                self.models.value = data.result!
            }
        }
    }
    func categoryAttachment(category: Int) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["category_id"] = category
        ApiManager.instance.connection(.category_attachments, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CategoryAttachmentModel.convertToModel(response: response)
            if(data.result != nil){
                self.attachments.value = data.result!
            }
        }
    }

}


