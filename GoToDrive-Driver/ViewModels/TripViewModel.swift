
import Foundation

class TripViewModel:ViewModelCore {
    enum TripStatus:Int {
        case opened = 0
        case accepted = 1
        case canceled = 2
        case rejected = 3
        case arrived = 4
        case started = 5
        case completed = 6
        case collected = 7
    }
    
    var currentTrip:DynamicType = DynamicType<TripResult>()
    var lastTrip:DynamicType = DynamicType<TripResult>()
    var cancelMessage:DynamicType = DynamicType<String>()
    var message:DynamicType = DynamicType<String>()
    var accepting:DynamicType = DynamicType<String>()
    var collecting:DynamicType = DynamicType<Bool>()
    var chatHandler:DynamicType = DynamicType<[ChatResult]>()
    
    func current() {
    
        delegate?.startLoading()
        ApiManager.instance.connection(.current_trip, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
            if data.rate_last_trip != nil {
                self.lastTrip.value = data.rate_last_trip!
            }
        }
    }
    func accept(price:String? = nil) {
        if price != nil {
            ApiManager.instance.paramaters["price"] = price!
        }
        delegate?.startLoading()
        ApiManager.instance.connection(.accept, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            if(data.message != nil){
                self.accepting.value = data.message!
            }
        }
    }
    func reject() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.reject, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            if(data.message != nil){
                self.message.value = data.message!
            }
        }
    }
    func arrive() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.arrive, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
        }
    }
    func start() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.start, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
        }
    }
    func startLearn(lat: Double, lng: Double) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["to_lat"] = lat
        ApiManager.instance.paramaters["to_lng"] = lng
        ApiManager.instance.connection(.startLearn, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
        }
    }
    func endLearn(lat: Double, lng: Double) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["to_lat"] = lat
        ApiManager.instance.paramaters["to_lng"] = lng
        ApiManager.instance.connection(.endLearn, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
        }
    }
    func complete(lat:Double,lng:Double) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["to_lat"] = lat
        ApiManager.instance.paramaters["to_lng"] = lng
        ApiManager.instance.connection(.complete, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = CurrentTripModel.convertToModel(response: response)
            if(data.result != nil){
                self.currentTrip.value = data.result!
            }
        }
    }
    func collect(trip:Int? = nil) {
        
        delegate?.startLoading()
        if trip != nil {
            ApiManager.instance.paramaters["trip_id"] = trip!
        }
        ApiManager.instance.connection(.collect, type: .get) { (response) in
            self.delegate?.stopLoading()
            let _ = BaseModel.convertToModel(response: response)
            self.collecting.value = true
        }
    }
    func rate(trip:Int,rate:Int,comment:String?) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["rate"] = rate
        if comment != nil {
            ApiManager.instance.paramaters["comment"] = comment!
        }
        let method = api(.rate,[trip])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            self.message.value = translate("success")
        }
    }
    func cancel(reason:Int) {
        
        delegate?.startLoading()
        ApiManager.instance.paramaters["cancel_reason_id"] = reason
        ApiManager.instance.connection(.cancel_request, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = BaseModel.convertToModel(response: response)
            if(data.message != nil){
                self.cancelMessage.value = data.message!
            }
        }
    }
    func chatRoom(userID: Int?, trip: Int?) {
        guard let userID = userID, let trip = trip else { return }
        delegate?.startLoading()
        ApiManager.instance.paramaters["trip_id"] = trip
        ApiManager.instance.paramaters["user_id"] = userID
        ApiManager.instance.connection(.chat, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = ChatModel.convertToModel(response: response)
            self.chatHandler.value = data.result
        }
    }
    func sendMessage(userID: Int?, trip: Int?, message: String?) {
        guard let userID = userID, let trip = trip, let message = message else { return }
        delegate?.startLoading()
        ApiManager.instance.paramaters["trip_id"] = trip
        ApiManager.instance.paramaters["user_id"] = userID
        ApiManager.instance.paramaters["message"] = message
        ApiManager.instance.connection(.send_message, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = ChatModel.convertToModel(response: response)
//            self.chatHandler.value = data.result
        }
    }
}


