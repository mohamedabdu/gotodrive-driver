
import Foundation

class UserViewModel:ViewModelCore {
    
    
    var history:DynamicType = DynamicType<HistoryResult>()
    var reviews:DynamicType = DynamicType<ReviewResult>()
    var model:DynamicType = DynamicType<UserRoot>()
    var message:DynamicType = DynamicType<String>()
    var notifications:DynamicType = DynamicType<[NotificationsResult]>()
    var home:DynamicType = DynamicType<[TripResult]>()
    var currentRequest:DynamicType = DynamicType<TripResult>()
    var adminRequest:DynamicType = DynamicType<AdminRequestResult>()
    var transactions:DynamicType = DynamicType<[AdminRequestResult]>()

    func fetchData() {
        delegate?.startLoading()
        ApiManager.instance.connection(.configs, type: .get) { (response) in
            self.delegate?.stopLoading()
            
        }
    }
    
    
    func login(username:String , password:String) {
        ApiManager.instance.paramaters["username"] = username
        ApiManager.instance.paramaters["password"] = password
        
        delegate?.startLoading()
        ApiManager.instance.connection(.login, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.access_token != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func register(paramters:[String:String]) {
        ApiManager.instance.paramaters = paramters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.register_step1, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.access_token != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func social(paramters:[String:String]) {
        ApiManager.instance.paramaters = paramters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.social, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.access_token != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func registerStep2(paramters:[String:Any]) {
        
        ApiManager.instance.paramaters = paramters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.register_step2, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            data.storeInDefault()
            self.model.value = data
        }
    }
    
    func update(paramters:[String:Any]) {
        ApiManager.instance.paramaters = paramters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.update, type: .post) { (response) in
            self.delegate?.stopLoading()
            let data = UserRoot.convertToModel(response: response)
            if data.result != nil {
                data.storeInDefault()
                self.model.value = data
            }
        }
    }
    func forget(email:String) {
        ApiManager.instance.paramaters["email"] = email
        
        delegate?.startLoading()
        ApiManager.instance.connection(.forget_password, type: .post) { (response) in
            self.delegate?.stopLoading()
            self.message.value = translate("password_reset_success")
        }
    }
    func reset(paramaters:[String:Any]) {
        ApiManager.instance.paramaters = paramaters
        
        delegate?.startLoading()
        ApiManager.instance.connection(.reset_password, type: .post) { (response) in
            self.delegate?.stopLoading()
            self.message.value = translate("password_reset_success")
        }
    }
    func fetchHistory() {
        delegate?.startLoading()
        ApiManager.instance.connection(.history, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = HistoryModel.convertToModel(response: response)
            if data.result != nil {
                self.history.value = data.result
            }
        }
    }
    func fetchReview() {
        delegate?.startLoading()
        ApiManager.instance.connection(.history_rates, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = ReviewModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result?.rates)
                self.reviews.value = data.result
            }
        }
    }
    func fetchNotifications() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.notifications, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = NotificationsModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result)
                self.notifications.value = data.result
            }
        }
    }
    func payToAdmin() {
        delegate?.startLoading()
        ApiManager.instance.connection(.payto_admin, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = AdminRequestModel.convertToModel(response: response)
            self.adminRequest.value = data.result
            
        }
    }
    func requestToAdmin() {
        delegate?.startLoading()
        ApiManager.instance.connection(.request_from_admin, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = AdminRequestModel.convertToModel(response: response)
            self.adminRequest.value = data.result
            
        }
    }
    func fetchTransactions() {
        
        delegate?.startLoading()
        ApiManager.instance.connection(.transactions, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = AdminRequestResultModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result)
                self.transactions.value = data.result
            }
        }
    }
    func fetchHome() {
        delegate?.startLoading()
        ApiManager.instance.connection(.home, type: .get) { (response) in
            self.delegate?.stopLoading()
            let data = TripModel.convertToModel(response: response)
            if data.result != nil {
                self.paginator(respnod: data.result)
                if data.current_request?.id != nil {
                    data.result?.insert(data.current_request!, at: 0)
                }
                self.home.value = data.result
            }
            
        }
    }
}


