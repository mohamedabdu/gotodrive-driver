//
//  ChatFromCell.swift
//  FashonDesign
//
//  Created by Mohamed Abdu on 5/30/18.
//  Copyright © 2018 Atiaf. All rights reserved.
//

import UIKit

class ChatToCell: UITableViewCell, CellProtocol {

    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(){
        viewParent.cornerRadius = 7
        viewParent.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMinYCorner]
        
        guard let data = model as? ChatResult else { return }
        
        guard let _ = data.message else { return }
        let user = UserRoot.getUserFromCache()
        userImage.setImage(url: user.result?.image)
        
        let size = data.message!.getSize()
        
        if size.width >= self.contentView.width{
            viewWidth.constant = self.contentView.width-30
        }else if size.width >= self.contentView.width/3{
            viewWidth.constant = viewWidth.constant-30
        }else{
            viewWidth.constant = size.width+15
        }
        
        
        if viewWidth.constant < self.contentView.width/3{
            viewWidth.constant = self.contentView.width/3
        }
        
       
        
        message.text = data.message
        date.text = Date.date(date: data.created_at, type: .hourlyM, usePM: true)

    }
  
}
