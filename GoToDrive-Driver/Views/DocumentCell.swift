//
//  DocumentCell.swift
//  NashmiDriver
//
//  Created by mohamed abdo on 2/5/19.
//  Copyright © 2019 Nashmi. All rights reserved.
//

import UIKit
protocol DocumentCellDelegate: class {
    func openPickerAtPath(for path: Int)
}

class DocumentCell: UITableViewCell, CellProtocol {

    @IBOutlet weak var attachmentName: UILabel!
    @IBOutlet weak var imageBtn: UIButton!
    
    weak var delegate: DocumentCellDelegate?
    var newImage: UIImage?
    func setup() {
        guard let model = model as? CategoryAttachmentResult else { return }
        attachmentName.text = model.name
        if newImage != nil {
            imageBtn.setImage(newImage, for: .normal)
        }
    }
    
    @IBAction func setImageAction(_ sender: Any) {
        delegate?.openPickerAtPath(for: indexPath())
    }
}
