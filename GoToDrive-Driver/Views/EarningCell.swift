//
//  EarningCell.swift
//  NashmiDriver
//
//  Created by mohamed abdo on 8/24/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

class EarningCell: UITableViewCell,CellProtocol {

   
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var arrivedTime: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    
    func setup() {
        guard let data = model as? TripResult else { return }
        userImage.setImage(url: data.client?.image)
        userName.text = "\(data.client?.first_name ?? "") \(data.client?.last_name ?? "")"
        arrivedTime.text = data.time_estimation
        totalPrice.text = translate(data.fare_estimation, "SAR")
        paymentMethod.text = data.payment_method_text
    }
    
}
