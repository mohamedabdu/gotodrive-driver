//
//  HistoryCell.swift
//  NashmiClient
//
//  Created by mohamed abdo on 8/21/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell , CellProtocol {

    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var typeGoods: UILabel!
    
    func setup() {
        guard let data = model as? TripResult else { return }
        userImage.setImage(url: data.client?.image)
        userName.text = "\(data.client?.first_name ?? "") \(data.client?.last_name ?? "")"
        createdDate.text = data.created_at
        totalPrice.text = translate(data.fare_estimation, "SAR")
        paymentMethod.text = data.payment_method_text
        status.text = data.status_text
        if data.status != nil && (data.status == TripViewModel.TripStatus.canceled.rawValue || data.status == TripViewModel.TripStatus.rejected.rawValue) {
            status.textColor = UIColor.colorRGB(red: 223, green: 71, blue: 83)
        }else{
            status.textColor = UIColor.colorRGB(red: 112, green: 178, blue: 102)
        }
        from.text = translate(data.from_location?.limit, "from_:")
        to.text = translate(data.to_location?.limit, "to_:")
        typeGoods.text = translate(data.trip_special_description?.limit, "type_of_goods_:")
    }
    
}
