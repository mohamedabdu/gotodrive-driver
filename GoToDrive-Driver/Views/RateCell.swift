//
//  RateCell.swift
//  NashmiDriver
//
//  Created by mohamed abdo on 8/24/18.
//  Copyright © 2018 Nashmi. All rights reserved.
//

import UIKit
import Cosmos

class RateCell: UITableViewCell,CellProtocol {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var endDate: UILabel!
    func setup() {
        guard let data = model as? ReviewRate else { return }
        userImage.setImage(url: data.client_image)
        userName.text = data.client_name
        comment.text = data.comment
        endDate.text = data.end_date
        guard let rate = data.rate?.double else { return }
        rateView.rating = rate
        
    }
    
}
