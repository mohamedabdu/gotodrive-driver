//
//  TransactionCell.swift
//  NashmiDriver
//
//  Created by mohamed abdo on 1/4/19.
//  Copyright © 2019 Nashmi. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell , CellProtocol {
    @IBOutlet weak var transactionTitle: UILabel!
    @IBOutlet weak var transactionCost: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    
   
    func setup() {
        guard let data = model as? AdminRequestResult else { return }
        transactionTitle.text = data.type_text
        transactionDate.text = data.created_at
        if data.type == "1"{
            transactionCost.text = data.admin_credit?.string
        }else{
            transactionCost.text = data.driver_credit?.string
        }
    }
    
}
